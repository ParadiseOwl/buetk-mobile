import * as React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import Home from "./Components/home";
import SignIn from "./Components/signin";
import Dashboard from "./Components/dashboard";

const Stack = createStackNavigator();

export default function MyStack(){
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions = {{headerShown: false}} initialRouteName = "Home" >
                <Stack.Screen name = "Home" component = {Home} />
                <Stack.Screen name = "SignIn" component = {SignIn} />
                <Stack.Screen name = "Dashboard" component = {Dashboard} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}