import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';
import Route from "./route";

import Home from "./Components/home";

const App: () => React$Node = () => {
  return (
    <>
      <Route />
    </> 
  );
};

export default App;
