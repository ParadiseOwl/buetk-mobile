import React, {Component} from "react";
import {View, StyleSheet, Image, ImageBackground, Text, Animated, Easing, TouchableWithoutFeedback, PanResponder} from "react-native";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "../ResponsiveFont";

//nested components
import Day from "./day";
import Month from "./month";
import Year from "./year";
import Hour from "./hour";
import Minute from "./minute";
import DayPeriod from "./dayperiod";
import LeaveInBalance from './leaveinbalance';

export default class AddLeaveRequest extends Component {
    pos = 0;
    dayLimit = 31;

    constructor(){
        super();
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateDate: new Animated.Value(0),
            animateDay: new Animated.Value(0),
            animateDayLeftArrow: new Animated.Value(0),
            animateDayText: new Animated.Value(0),
            animateDayRightArrow: new Animated.Value(0),
            animateMonth: new Animated.Value(0),
            animateMonthLeftArrow: new Animated.Value(0),
            animateMonthText: new Animated.Value(0),
            animateMonthRightArrow: new Animated.Value(0),
            animateYear: new Animated.Value(0),
            animateYearLeftArrow: new Animated.Value(0),
            animateYearRightArrow: new Animated.Value(0),
            animateYearText: new Animated.Value(0),
            animateModal: new Animated.Value(0),
            animateDateTimePicker: new Animated.Value(0),
            animateDateItems: [],
            animateDropDownMenu: new Animated.Value(0),
            animatePanResponder: new Animated.Value(0),
            animateSubmitButton: new Animated.Value(0),
            animateSubmitButtonText: new Animated.Value(0),

            //standard variables
            Year: 2020,
            dateitem: [],
            leaveType: ['Casual Leave', 'Annual Leave', 'Paid Leave'],
            selectedLeave: 1,

            //load variables
            dayEnabled: false,
            dropDownEnabled: false,
        }
        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                if(gestureState.dy < -30){
                    return true;
                }
            },

            onPanResponderMove: (evt, gestureState) => {
                if(gestureState.dy + this.pos < 0 && gestureState.dy + this.pos > -75){
                    this.state.animatePanResponder.setValue(gestureState.dy);
                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                this.pos = -75;
                if(gestureState.dy < -50){
                    Animated.timing(this.state.animatePanResponder, {
                        toValue: -75,
                        duration: 400,
                        easing: this.state.easing,
                        useNativeDriver: true,
                    }).start(()=> {
                        this.AnimateSubmitButton();
                    });
                }
                else{
                    this.pos = 0;
                    Animated.timing(this.state.animatePanResponder, {
                        toValue: 0,
                        duration: 400,
                        easing: this.state.easing,
                        useNativeDriver: true,
                    }).start();
                }
            }
        })
    }

    componentDidMount(){

    }
    
    IncreaseMinutes(type){
        let minute = 0;
        if(type){
            minute = this.refs.timeoutminute.GetValue();
        }
        else {
            minute = this.refs.timeinminute.GetValue();
        }
        if(minute === 60){
            minute = 0;
        }
        else {
            minute++;
        }
        if(type){
            this.refs.timeoutminute.SetValue(minute);
        }
        else{
            this.refs.timeinminute.SetValue(minute);
        }
    }
    
    DecreaseMinutes(type){
        let minute = 0;
        if(type){
            minute = this.refs.timeoutminute.GetValue();
        }
        else {
            minute = this.refs.timeinminute.GetValue();
        }
        if(minute === 0){
            minute = 60;
        }
        else {
            minute--;
        }
        if(type){
            this.refs.timeoutminute.SetValue(minute);
        }
        else {
            this.refs.timeinminute.SetValue(minute);
        }
    }

    IncreaseHour(type){
        let hour = 0;
        if(type) hour = this.refs.timeouthour.GetValue();
        else hour = this.refs.timeinhour.GetValue();
        if(hour === 12) hour = 1;
        else hour++;
        if(type) this.refs.timeouthour.SetValue(hour);
        else this.refs.timeinhour.SetValue(hour);
    }

    DecreaseHour(type){
        let hour = 0;
        if(type) hour = this.refs.timeouthour.GetValue();
        else hour = this.refs.timeinhour.GetValue();
        if(hour === 1) hour = 12;
        else hour--;
        if(type) this.refs.timeouthour.SetValue(hour);
        else this.refs.timeinhour.SetValue(hour);
    }

    IncreaseDay(type){
        let day = 0, limit = this.dayLimit;
        if(type){
            day = this.refs.fromdayreference.GetValue();
        }
        else{
            day = this.refs.todayreference.GetValue();
        }
        if(day === limit){
            day = 1;
        }
        else{
            day++;
        }
        if(type){
            this.refs.fromdayreference.SetValue(day);
        }
        else{
            this.refs.todayreference.SetValue(day);
        }
    }

    DecreaseDay(type){
        let day = 0, limit = this.dayLimit;
        if(type){
            day = this.refs.fromdayreference.GetValue();
        }
        else{
            day = this.refs.todayreference.GetValue();
        }
        if(day === 1){
            day = limit;
        }
        else {
            day--;
        }
        if(type) this.refs.fromdayreference.SetValue(day);
        else this.refs.todayreference.SetValue(day);
    }
    
    IncreaseMonth(type){
        let month = 1, monthSetOne = [1, 3, 5, 7, 8, 10, 12], limit;
        if(type) month = this.refs.frommonthreference.GetMonthValue();
        else month = this.refs.tomonthreference.GetMonthValue();
        if(month === 12){
            month = 1;
        }
        else{
            month++;
        }

        if(monthSetOne.includes(month)){
            limit = 31;
            
        }
        else if(month === 2){
            limit = 28;
        }
        else{
            limit = 30;
        }
        if(type){
            this.refs.fromdayreference.SetValue(1);
            this.refs.frommonthreference.SetMonthValue(month, this.GetMonthName(month));
        }
        else{
            this.refs.todayreference.SetValue(1);
            this.refs.tomonthreference.SetMonthValue(month, this.GetMonthName(month));
        }
        this.dayLimit = limit;
    }

    DecreaseMonth(type){
        let month = 1, monthSetOne = [1, 3, 5, 7, 8, 10, 12], limit;
        if(type) month = this.refs.frommonthreference.GetMonthValue();
        else month = this.refs.tomonthreference.GetMonthValue();
        if(month === 1){
            month = 12;
        }
        else{
            month--;
        }
        if(monthSetOne.includes(month)){
            limit = 31;
        }
        else if(month === 2){
            limit = 28;
        }
        else{
            limit = 30;
        }
        if(type){
            this.refs.fromdayreference.SetValue(1);
            this.refs.frommonthreference.SetMonthValue(month, this.GetMonthName(month));
        }
        else {
            this.refs.todayreference.SetValue(1);
            this.refs.tomonthreference.SetMonthValue(month, this.GetMonthName(month));
        }
        this.dayLimit = limit
    }

    IncreaseYear(type){
        let year = 0;
        if(type) {
            year = this.refs.fromyearreference.GetValue();
        }
        else {
            year = this.refs.toyearreference.GetValue();
        }
        year++;
        if(type) this.refs.fromyearreference.SetValue(year);
        else this.refs.toyearreference.SetValue(year);
    }

    DecreaseYear(type){
        let year = 0;
        if(type){
            year = this.refs.fromyearreference.GetValue();
        }
        else {
            year = this.refs.toyearreference.GetValue();
        }
        year--;
        if(type) this.refs.fromyearreference.SetValue(year);
        else this.refs.toyearreference.SetValue(year);
    }

    ChangeDayPeriod(type){
        if(type) this.refs.timeoutdayperiod.ChangeValue();
        else this.refs.timeindayperiod.ChangeValue();
    }

    GetMonthName(month){
        if(month === 1) return "Jan";
        else if(month === 2) return "Feb";
        else if(month === 3) return "Mar";
        else if(month === 4) return "Apr";
        else if(month === 5) return "May";
        else if(month === 6) return "Jun";
        else if(month === 7) return "Jul";
        else if(month === 8) return "Aug";
        else if(month === 9) return "Sep";
        else if(month === 10) return "Oct";
        else if(month === 11) return "Nov";
        else if(month === 12) return "Dec";
    }   

    SetDateTimeItems(){
        let limit = this.dayLimit, array = [], animatedArray = [];
        for(let a = 0; a < limit; a++){
            array.push(a + 1)
            animatedArray.push(new Animated.Value(0));
        }
        this.setState({
            dateitem: array,
            animateDateItems: animatedArray
        });
    }

    SetDateFromDateTime(item){
        let day = item;
        this.ResetDateTimePicker(day);
    }

    ChangeLeaveType(type){
        let leaveType = this.state.leaveType;
        let swap = "";
        if(type){
            swap = leaveType[1];
            leaveType[1] = leaveType[0];
            leaveType[0] = swap;
        }
        else{
            swap = leaveType[2];
            leaveType[2] = leaveType[0];
            leaveType[0] = swap;
        }
        this.setState({
            leaveType
        })
    }

    //Animated functions here
    AnimateDayButton(alt){
        this.ResetButtons(false, true, true);
        Animated.sequence([
            Animated.timing(this.state.animateDay, {
                toValue: alt ? 100 : -100,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateDay, {
                toValue: alt ? 200 : -200,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateDayLeftArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDayRightArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDayText, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ]).start(()=> {
            this.setState({dayEnabled: true});
        });
    }

    ResetDayButton(){
        Animated.sequence([
            Animated.stagger(100, [
                Animated.timing(this.state.animateDayLeftArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDayRightArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDayText, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateDay, {
                toValue: 0,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start(()=> {
            this.setState({
                dayEnabled: false,
            })
        });
    }

    AnimateMonthButton(alt){
        this.ResetButtons(true, false, true);
        Animated.sequence([
            Animated.timing(this.state.animateMonth, {
                toValue: alt ? 100 : -100,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateMonth, {
                toValue: alt ? 200 : -200,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateMonthLeftArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthRightArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthText, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ]).start();
    }

    ResetMonthButton(){
        Animated.sequence([
            Animated.stagger(100, [
                Animated.timing(this.state.animateMonthLeftArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthRightArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthText, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateMonth, {
                toValue: 0,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateYearButton(alt){
        this.ResetButtons(true, true, false);
        Animated.sequence([
            Animated.timing(this.state.animateYear, {
                toValue: alt ? 100 : -100,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateYear, {
                toValue: alt ? 200 : -200,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateYearLeftArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearRightArrow, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearText, {
                    toValue: alt ? 100 : -100,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ]).start();
    }

    ResetYearButton(){
        Animated.sequence([
            Animated.stagger(100, [
                Animated.timing(this.state.animateYearLeftArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearRightArrow, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearText, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateYear, {
                toValue: 0,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start();
    }

    AnimateDateTime(){
        Animated.sequence([
            Animated.timing(this.state.animateModal, {
                toValue: 100,
                duration: 0,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateModal, {
                    toValue: 200,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDateTimePicker, {
                    toValue: 100,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
        ]).start(()=> {
            this.SetDateTimeItems();
            this.AnimateDateItems();
        });
    }   

    ResetDateTimePicker(day){
        if(day != 0){
            this.refs.fromdayreference.SetValue(day);
        }
        let animations = [];
        this.state.animateDateItems.forEach(items => {
            animations.push(Animated.timing(items, {
                toValue: 0,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }));
        });

        Animated.sequence([
            Animated.parallel(animations),
            Animated.stagger(100, [
                Animated.timing(this.state.animateDateTimePicker, {
                    toValue: 0,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateModal, {
                    toValue: 100,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateModal, {
                toValue: 0,
                duration: 0,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateDateItems = (value = 1) => {
        let animation = [];
        this.state.animateDateItems.forEach(item => {
            animation.push(Animated.timing(item, {
                toValue: value,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }));
        });

        Animated.stagger(50, animation).start();
    }

    ResetButtons(day, month, year){
        if(day){
            this.ResetDayButton();
        }
        if(month){
            this.ResetMonthButton();
        }
        if(year){
            this.ResetYearButton();
        }
    }

    ToggleDropDownMenu(type){
        let enabled = this.state.dropDownEnabled;
        if(enabled){
            Animated.timing(this.state.animateDropDownMenu, {
                toValue: 0,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true
            }).start(
                ()=> {
                    if(type !== null){
                        this.ChangeLeaveType(type);
                    }
                    this.setState({
                        dropDownEnabled: !enabled
                    })
                }
            );
        }
        else{
            Animated.timing(this.state.animateDropDownMenu, {
                toValue: 100,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true
            }).start(()=> {
                this.setState({
                    dropDownEnabled: !enabled
                })
            });
        }
    }

    AnimateSubmitButton(){
        let year = this.refs.fromyearreference.GetValue();
        let month = this.refs.frommonthreference.GetMonthValue();
        let day = this.refs.fromdayreference.GetValue();
        this.refs.leaveinbalance.GetLeaveInBalance(day, month, year)
        Animated.stagger(100, [
            Animated.timing(this.state.animateSubmitButton, {
                toValue: 100,
                duration: 600,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateSubmitButtonText, {
                toValue: 100,
                duration: 600,
                easing: this.state.easing,
                useNativeDriver: true
            })
        ]).start();
    }

    ResetSubmitButton(){
        this.pos = 0;
        Animated.stagger(100, [
            Animated.timing(this.state.animateSubmitButtonText, {
                toValue: 0,
                duration: 600,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateSubmitButton, {
                toValue: 0,
                duration: 600,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animatePanResponder, {
                toValue: 0,
                duration: 600,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    render(){
        return(
            <View style = {styles.container} {...this._panResponder.panHandlers} >
                <Animated.View style = {[styles.viewport, {transform: [{translateY: this.state.animatePanResponder.interpolate({
                    inputRange: [-75, 0],
                    outputRange: [-50, 0]
                })}, {scale: this.state.animatePanResponder.interpolate({
                    inputRange: [-75, 0],
                    outputRange: [0.8, 1]
                })}], }]}>

                    {/* To date portion */}
                    {/* starts here */}
                    <View style = {[styles.datecontainer, {left: '5%',}]}>
                        <View style = {styles.dateheadercontainer}>
                            <Text style = {styles.dateheader}>From Date</Text>
                        </View>
                        <TouchableWithoutFeedback onPress = {this.AnimateDayButton.bind(this, true)}>
                            <View style = {[styles.dateinputcontainer, {top: '20%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {transform: [{scale: this.state.animateDay.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0.7, 0.7, 0.7, 1, 1]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {opacity: this.state.animateDay.interpolate({
                                        inputRange:[-200, -100, 0, 100, 200],
                                        outputRange: [1, 1, 1, 0, 0]
                                    }), transform: [{translateY: this.state.animateDay.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 0, -10, -10]
                                    })}]}]}>Day</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {opacity: this.state.animateDay.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0, 0, 0, 0, 1]
                                })}]}>
                                    <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateDayLeftArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, 0, 1]
                                    }), transform: [{translateY: this.state.animateDayLeftArrow.interpolate({
                                        inputRange: [-100, 0, 100], 
                                        outputRange: [-10 ,-10, 0]
                                    })}]}]}>
                                        <Image style = {styles.arrow} resizeMode = "contain" source = {require('../resources/arrowleft.png')} />
                                    </Animated.View>
                                    {this.state.dayEnabled && (
                                        <TouchableWithoutFeedback onPress = {this.DecreaseDay.bind(this, true)}>
                                            <View style = {[styles.dateinputalternativearrowcontainer, {left: '0%',}]} />
                                        </TouchableWithoutFeedback>
                                    )}
                                    <Animated.View style = {[styles.dateinputalternativearrowcontainer, {right: '0%', opacity: this.state.animateDayRightArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, 0, 1]
                                    }), transform: [{translateY: this.state.animateDayRightArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [-10, -10, 0]
                                    })}]}]}>
                                        <Image style = {[styles.arrow, {transform: [{rotate: '180deg'}, {scale: 0.65}]}]} resizeMode = "contain" source = {require('../resources/arrowleft.png')} />
                                    </Animated.View>
                                    {this.state.dayEnabled && (
                                        <TouchableWithoutFeedback onPress = {this.IncreaseDay.bind(this, true)}>
                                            <View style = {[styles.dateinputalternativearrowcontainer, {right: '0%',}]} />
                                        </TouchableWithoutFeedback>
                                    )}
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateDayText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, 0, 1]
                                    }), transform: [{translateY: this.state.animateDayText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [-10, -10, 0]
                                    })}]}]}>
                                        <Day ref =  'fromdayreference' />
                                    </Animated.View>
                                    {this.state.dayEnabled && <TouchableWithoutFeedback onPress = {this.AnimateDateTime.bind(this)}>
                                        <View style = {styles.datetimepickertouch} />
                                    </TouchableWithoutFeedback>}
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.AnimateMonthButton.bind(this, true)}>
                            <View style = {[styles.dateinputcontainer, {top: '45%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {transform: [{scale: this.state.animateMonth.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0.7, 0.7, 0.7, 1, 1]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {opacity: this.state.animateMonth.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [1, 1, 1, 0, 0]
                                    }), transform: [{translateY: this.state.animateMonth.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 0, -10, -10]
                                    })}]}]}>Month</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {opacity: this.state.animateMonth.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0, 0, 0, 0, 1]
                                })}]}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseMonth.bind(this, true)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [1, 0, 1]
                                        }), transform: [{translateY: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [-10, -10, 0]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} style = {styles.arrow} resizeMode = "contain" />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseMonth.bind(this, true)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '70%', opacity: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, 0, 1]
                                        }), transform: [{translateY: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [-10, -10, 0]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} style = {[styles.arrow, {transform: [{scale: 0.65}, {rotate: '180deg'}]}]} resizeMode = "contain" />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateMonthText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 1]
                                    }), transform: [{translateY: this.state.animateMonthText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [-10, -10, 0]
                                    })}]}]}>
                                        <Month ref = "frommonthreference" />
                                    </Animated.View>
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.AnimateYearButton.bind(this, true)}>
                            <View style = {[styles.dateinputcontainer, {top: '70%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {transform: [{scale: this.state.animateYear.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0.7, 0.7, 0.7, 1, 1]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {opacity: this.state.animateYear.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [1, 1, 1, 0, 0]
                                    }), transform: [{translateY: this.state.animateYear.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 0, -10, -10]
                                    })}]}]}>Year</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {opacity: this.state.animateYear.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [0, 0, 0, 0, 1]
                                })}]}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseYear.bind(this, true)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, 0, 1]
                                        }), transform: [{translateY: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [-10, -10, 0]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {styles.arrow} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseYear.bind(this, true)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {right: '0%', opacity: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, 0, 1]
                                        }), transform: [{translateY: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [-10, -10, 0]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.arrow, {transform: [{rotate: '180deg'}, {scale: 0.65}]}]} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateYearText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 1]
                                    }), transform: [{translateY: this.state.animateYearText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [-10, -10, 0]
                                    })}]}]}>
                                        <Year ref = "fromyearreference" />
                                    </Animated.View>
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    {/* from date portion */}
                    {/* starts here */}
                    <View style = {[styles.datecontainer, {right: '5%', backgroundColor: '#323232',}]}>
                        <View style = {styles.dateheadercontainer}>
                            <Text style = {[styles.dateheader, {color: 'white'}]}>To Date</Text>
                        </View>
                        <TouchableWithoutFeedback onPress = {this.AnimateDayButton.bind(this, false)}>
                            <View style = {[styles.dateinputcontainer, {top: '20%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {borderColor: 'white', transform: [{scale: this.state.animateDay.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 1, 0.7, 0.7, 0.7]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {color: 'white' ,opacity: this.state.animateDay.interpolate({
                                        inputRange:[-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 1, 1, 1]
                                    }), transform: [{translateY: this.state.animateDay.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [-10, -10, 0, 0, 0]
                                    })}]}]}>Day</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {backgroundColor: 'white' ,opacity: this.state.animateDay.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 1, 0, 0 ,0]
                                })}]}>
                                    <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateDayLeftArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 0]
                                    }), transform: [{translateY: this.state.animateDayLeftArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, -10, -10]
                                    })}]}]}>
                                        <Image style = {[styles.arrow, {tintColor: '#323232'}]} resizeMode = "contain" source = {require('../resources/arrowleft.png')} />
                                    </Animated.View>
                                    {this.state.dayEnabled && (
                                        <TouchableWithoutFeedback onPress = {this.DecreaseDay.bind(this, false)}>
                                            <View style = {[styles.dateinputalternativearrowcontainer, {left: '0%',}]} />
                                        </TouchableWithoutFeedback>
                                    )}
                                    <Animated.View style = {[styles.dateinputalternativearrowcontainer, {right: '0%', opacity: this.state.animateDayRightArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 0]
                                    }), transform: [{translateY: this.state.animateDayRightArrow.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, -10, -10]
                                    })}]}]}>
                                        <Image style = {[styles.arrow, {tintColor: '#323232' ,transform: [{rotate: '180deg'}, {scale: 0.65}]}]} resizeMode = "contain" source = {require('../resources/arrowleft.png')} />
                                    </Animated.View>
                                    {this.state.dayEnabled && (
                                        <TouchableWithoutFeedback onPress = {this.IncreaseDay.bind(this, false)}>
                                            <View style = {[styles.dateinputalternativearrowcontainer, {right: '0%',}]} />
                                        </TouchableWithoutFeedback>
                                    )}
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateDayText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 0]
                                    }), transform: [{translateY: this.state.animateDayText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, -10, -10]
                                    })}]}]}>
                                        <Day ref =  'todayreference' whiteversion = {true} />
                                    </Animated.View>
                                    {this.state.dayEnabled && <TouchableWithoutFeedback onPress = {this.AnimateDateTime.bind(this)}>
                                        <View style = {styles.datetimepickertouch} />
                                    </TouchableWithoutFeedback>}
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.AnimateMonthButton.bind(this, false)}>
                            <View style = {[styles.dateinputcontainer, {top: '45%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {borderColor: 'white' ,transform: [{scale: this.state.animateMonth.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 1, 0.7, 0.7, 0.7]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {color: 'white' ,opacity: this.state.animateMonth.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 1, 1, 1]
                                    }), transform: [{translateY: this.state.animateMonth.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [-10, -10, 0, 0, 0]
                                    })}]}]}>Month</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {backgroundColor: 'white' ,opacity: this.state.animateMonth.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 0, 0, 0, 0]
                                })}]}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseMonth.bind(this, false)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [1, 0, 0]
                                        }), transform: [{translateY: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, -10, -10]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} style = {[styles.arrow, {tintColor: '#323232'}]} resizeMode = "contain" />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseMonth.bind(this, false)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '70%', opacity: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [1, 0, 0]
                                        }), transform: [{translateY: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, -10, -10]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} style = {[styles.arrow, {tintColor: '#323232' ,transform: [{scale: 0.65}, {rotate: '180deg'}]}]} resizeMode = "contain" />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateMonthText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 0]
                                    }), transform: [{translateY: this.state.animateMonthText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, -10, -10]
                                    })}]}]}>
                                        <Month ref = "tomonthreference" whiteversion = {true} />
                                    </Animated.View>
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.AnimateYearButton.bind(this, false)}>
                            <View style = {[styles.dateinputcontainer, {top: '70%',}]}>
                                <Animated.View style = {[styles.dateinputnormalcontainer, {borderColor: 'white' ,transform: [{scale: this.state.animateYear.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 1, 0.7, 0.7, 0.7]
                                })}]}]}>
                                    <Animated.Text style = {[styles.dateinputnormaltext, {color: 'white' ,opacity: this.state.animateYear.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [0, 0, 1, 1, 1]
                                    }), transform: [{translateY: this.state.animateYear.interpolate({
                                        inputRange: [-200, -100, 0, 100, 200],
                                        outputRange: [-10, -10, 0, 0, 0]
                                    })}]}]}>Year</Animated.Text>
                                </Animated.View>
                                <Animated.View style = {[styles.dateinputalternativecontainer, {backgroundColor: 'white' ,opacity: this.state.animateYear.interpolate({
                                    inputRange: [-200, -100, 0, 100, 200],
                                    outputRange: [1, 1, 0, 0, 0]
                                })}]}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseYear.bind(this, false)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {left: '0%', opacity: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [1, 0, 0]
                                        }), transform: [{translateY: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, -10, -10]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.arrow, {tintColor: '#323232'}]} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseYear.bind(this, false)}>
                                        <Animated.View style = {[styles.dateinputalternativearrowcontainer, {right: '0%', opacity: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [1, 0, 0]
                                        }), transform: [{translateY: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [-100, 0, 100],
                                            outputRange: [0, -10, -10]
                                        })}]}]}>
                                            <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.arrow, {tintColor: '#323232' ,transform: [{rotate: '180deg'}, {scale: 0.65}]}]} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputalternativetextcontainer, {opacity: this.state.animateYearText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [1, 0, 0]
                                    }), transform: [{translateY: this.state.animateYearText.interpolate({
                                        inputRange: [-100, 0, 100],
                                        outputRange: [0, -10, -10]
                                    })}]}]}>
                                        <Year ref = "toyearreference" whiteversion = {true} />
                                    </Animated.View>
                                </Animated.View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    {/* Leave Type portion */}
                    {/* starts here */}
                    <View style = {styles.leavetypecontainer}>
                        <View style = {styles.leavetypeheadingcontainer}>
                            <Text style = {styles.leavetypeheading}>Leave Type</Text>
                        </View>
                        <TouchableWithoutFeedback onPress = {this.ToggleDropDownMenu.bind(this, null)}>
                            <View style = {styles.leavetypeselectedcontainer}>
                                <Animated.View style = {[styles.leavetypeselectedcover, {opacity: this.state.animateDropDownMenu.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0, 1]
                                })}]} />
                                <Animated.Text style = {[styles.leavetypeselectedtext, {opacity: this.state.animateDropDownMenu.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [1, 0]
                                })}]}>{this.state.leaveType[0]}</Animated.Text>
                                <Animated.Text style = {[styles.leavetypeselectedtext, {color: '#323232', opacity: this.state.animateDropDownMenu.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0, 1]
                                })}]}>{this.state.leaveType[0]}</Animated.Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <Animated.View style = {[styles.leavetypedropdowncontainer, {transform: [{translateY: this.state.animateDropDownMenu.interpolate({
                            inputRange: [0, 100],
                            outputRange: [-GetScreenHeight(6), 0]
                        })}, {scaleY: this.state.animateDropDownMenu.interpolate({
                            inputRange: [0, 100],
                            outputRange: [0.01, 1]
                        })}]}]}>
                            <View style = {[styles.leavetypedropdowncontent, {top: '0%', borderBottomWidth: 1, borderBottomColor: '#323232'}]}>
                                <Text style = {[styles.leavetypeselectedtext, {color: '#323232'}]}>{this.state.leaveType[1]}</Text>
                            </View>
                            <View style = {[styles.leavetypedropdowncontent, {top: '50%',}]}>
                                <Text style = {[styles.leavetypeselectedtext, {color: '#323232'}]}>{this.state.leaveType[2]}</Text>
                            </View>
                        </Animated.View>
                    </View>
                    {this.state.dropDownEnabled && (
                        <View style = {[styles.leavetypedropdowncontainer, {backgroundColor: 'transparent', borderWidth: 0, top: '66.25%'}]}>
                            <TouchableWithoutFeedback onPress = {this.ToggleDropDownMenu.bind(this, true)}>
                                <View style = {[styles.leavetypedropdowncontent, {top: '0%',}]} />
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress = {this.ToggleDropDownMenu.bind(this, false)}>
                                <View style = {[styles.leavetypedropdowncontent, {top: '50%'}]} />
                            </TouchableWithoutFeedback>
                        </View>
                    )}

                    {/* Time out portion */}
                    {/* starts here */}
                    <Animated.View style = {[styles.timeoutcontainer]}>
                        <View style = {styles.timeheadercontainer}>
                            <Text style = {[styles.timeheader, {color: 'white'}]}>Time Out</Text>
                        </View>
                        <View style = {styles.timebuttonscontainer}>
                            <View style = {[styles.timebuttonscolumn, {left: '0%'}]}>
                                <TouchableWithoutFeedback onPress = {this.IncreaseHour.bind(this, true)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {top: '0%',}]}>
                                        <Image style = {[styles.timearrow, {tintColor: 'white', transform: [{rotate: '90deg'}]}]} source = {require('../resources/arrowleft.png')} resizeMode = "contain" />
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style = {[styles.timebuttontimecontainer]}>
                                    <Hour color = 'white' ref = "timeouthour" />
                                </View> 
                                <TouchableWithoutFeedback onPress = {this.DecreaseHour.bind(this, true)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {bottom: '0%',}]}>
                                        <Image style = {[styles.timearrow, {tintColor: 'white', transform: [{rotate: '-90deg'}]}]} source = {require('../resources/arrowleft.png')} resizeMode = "contain" />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style = {styles.coloncontainer}>
                                <Text style = {styles.colon}>:</Text>
                            </View> 
                            <View style = {[styles.timebuttonscolumn, {left: '33%'}]}>
                                <TouchableWithoutFeedback onPress = {this.IncreaseMinutes.bind(this, true)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {top: '0%',}]}>
                                        <Image style = {[styles.timearrow, {tintColor: 'white', transform: [{rotate: '90deg'}]}]} source = {require('../resources/arrowleft.png')} resizeMode = "contain" />
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style = {[styles.timebuttontimecontainer]}>
                                    <Minute color = "white" ref = "timeoutminute" />
                                </View> 
                                <TouchableWithoutFeedback onPress = {this.DecreaseMinutes.bind(this, true)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {bottom: '0%',}]}>
                                        <Image style = {[styles.timearrow, {tintColor: 'white', transform: [{rotate: '-90deg'}]}]} source = {require('../resources/arrowleft.png')} resizeMode = "contain" />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style = {[styles.timebuttonscolumn, {left: '66%'}]}>
                                <TouchableWithoutFeedback onPress = {this.ChangeDayPeriod.bind(this, true)}>
                                    <View style = {[styles.timebuttontimecontainer, {backgroundColor: 'white'}]}>
                                        <DayPeriod color = "#323232" ref = "timeoutdayperiod" />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </Animated.View>
                    
                    {/* Time in portion */}
                    {/* starts here */}
                    <Animated.View style = {[styles.timeincontainer, {transform: [{translateY: this.state.animateDropDownMenu.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, GetScreenHeight(10)]
                    })}]}]}>
                        <View style = {styles.timeheadercontainer}>
                            <Text style = {[styles.timeheader, {color: '#323232'}]}>Time In</Text>
                        </View>
                        <View style = {styles.timebuttonscontainer}>
                            <View style = {[styles.timebuttonscolumn, {left: '0%', }]}>
                                <TouchableWithoutFeedback onPress = {this.IncreaseHour.bind(this, false)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {top: '0%',}]}>
                                        <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.timearrow, {tintColor: '#323232', transform: [{rotate: '90deg'}]}]} />
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style = {[styles.timebuttontimecontainer, {borderColor: '#323232'}]}>
                                    <Hour color = "#323232" ref = "timeinhour" />
                                </View>
                                <TouchableWithoutFeedback onPress = {this.DecreaseHour.bind(this, false)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {bottom: '0%'}]}>
                                        <Image style = {[styles.timearrow, {tintColor: '#323232', transform: [{rotate: '-90deg'}]}]} resizeMode = "contain" source = {require('../resources/arrowleft.png')} />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style = {styles.coloncontainer}>
                                <Text style = {[styles.colon, {color: '#323232'}]}>:</Text>
                            </View>
                            <View style = {[styles.timebuttonscolumn, {left: '33%'}]}>
                                <TouchableWithoutFeedback onPress = {this.IncreaseMinutes.bind(this, false)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {top: '0%'}]}>
                                        <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.timearrow, {tintColor: '#323232', transform: [{rotate: '90deg'}]}]} />
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style = {[styles.timebuttontimecontainer, {borderColor: '#323232'}]}>
                                    <Minute color = "#323232" ref = "timeinminute" />
                                </View>
                                <TouchableWithoutFeedback onPress = {this.DecreaseMinutes.bind(this, false)}>
                                    <View style = {[styles.timebuttonarrowcontainer, {bottom: '0%',}]}>
                                        <Image source = {require('../resources/arrowleft.png')} resizeMode = "contain" style = {[styles.timearrow, {tintColor: '#323232', transform: [{rotate: '-90deg'}]}]} />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style = {[styles.timebuttonscolumn, {left: '66%'}]}>
                                <TouchableWithoutFeedback onPress = {this.ChangeDayPeriod.bind(this, false)}>
                                    <View style = {[styles.timebuttontimecontainer, {backgroundColor: '#323232'}]}>
                                        <DayPeriod color = "white" ref = "timeindayperiod" />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </Animated.View>
                    
                    {/* modal and date picker portion */}
                    {/* starts here */}
                    <TouchableWithoutFeedback onPress = {this.ResetDateTimePicker.bind(this, 0)}>
                        <Animated.View style = {[styles.datetimepickermodal, {opacity: this.state.animateModal.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 0, 0.75]
                        }), transform: [{translateX: this.state.animateModal.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-GetScreenWidth(100), 0, 0]
                        })}]}]} />
                    </TouchableWithoutFeedback>
                    <Animated.View style = {[styles.datetimepickercontainer, {opacity: this.state.animateDateTimePicker.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateY: this.state.animateDateTimePicker.interpolate({
                        inputRange: [0, 100],
                        outputRange: [GetScreenHeight(50), 0]
                    })}]}]}>
                        <View style = {styles.dateitemscontainer}>
                            {this.state.dateitem.map((item, index) => (
                                <TouchableWithoutFeedback onPress = {this.SetDateFromDateTime.bind(this, item)}>
                                    <Animated.View style = {[styles.dateitem, {opacity: this.state.animateDateItems[index], transform: [{translateY: this.state.animateDateItems[index].interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [-10, 0]
                                    })}]}]}>
                                        <Text style = {[styles.dateitemtext, {color: 'white'}]}>{item}</Text>
                                    </Animated.View>
                                </TouchableWithoutFeedback>
                            ))}
                        </View>
                    </Animated.View>
                    
                    {/* Viewport cover to block inputs */}
                    {/* starts here */}
                    <TouchableWithoutFeedback onPress = {this.ResetSubmitButton.bind(this)}>
                        <Animated.View style = {[styles.viewport, {transform: [{translateX: this.state.animateSubmitButton.interpolate({
                            inputRange: [0, 100],
                            outputRange: [GetScreenWidth(100), 0]
                        })}]}]} />
                    </TouchableWithoutFeedback>
                </Animated.View>
                
                {/* submit button */}
                {/* starts here */}
                <Animated.View style = {[styles.footer, {transform: [{translateY: this.state.animatePanResponder.interpolate({
                    inputRange: [-75, 0],
                    outputRange: [0, GetScreenHeight(15)]
                })}]}]}>
                    <View style = {[styles.bottomline]}>
                        <View style = {[styles.sendbuttoncontainer, {left: '5%'}]}>
                            <Image style = {styles.sendbuttonimage} resizeMode = "contain" source = {require('../resources/done.png')} />
                            <Text style = {styles.sendbuttontext}>Send</Text>
                        </View>
                        <View style = {styles.leaveinbalancecontainer}>
                            <View style = {styles.leaveinbalanceheadercontainer}>
                                <Animated.Text style = {[styles.leaveinbalancetext, {bottom: '5%', transform: [{translateX: this.state.animateSubmitButton.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [-10, 0]
                                })}], opacity: this.state.animateSubmitButton.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0, 1]
                                })}]}>Leave In Balance</Animated.Text>
                            </View>
                            <Animated.View style = {[styles.leaveinbalanceamountcontainer, {opacity: this.state.animateSubmitButtonText.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, 1]
                            }), transform: [{translateX: this.state.animateSubmitButtonText.interpolate({
                                inputRange: [0, 100],
                                outputRange: [10, 0]
                            })}]}]}>
                                <LeaveInBalance ref = "leaveinbalance" loginData = {this.props.loginData} />
                            </Animated.View>
                        </View>
                        <TouchableWithoutFeedback onPress = {this.ResetSubmitButton.bind(this)}>
                            <View style = {[styles.sendbuttoncontainer, {backgroundColor: '#f75d4f', right: '5%'}]}>
                                <Image style = {styles.sendbuttonimage} resizeMode = "contain" source = {require('../resources/close.png')} />
                                <Text style = {styles.sendbuttontext}>Close</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                 </Animated.View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '70%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '30%',
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    datecontainer: {
        height: '50%',
        width: '42.5%',
        position: 'absolute',
        top: '0%',
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: GetScreenHeight(1),
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateheadercontainer: {
        height: '15%',
        width: '95%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        justifyContent: 'center'
    },
    dateheader: {
        color: '#323232',
        fontSize: DynamicFont(60),
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute',
        bottom: '0%',
    },
    dateinputcontainer: {
        height: '22.5%',
        width: '90%',
        position: 'absolute',
        left: '5%',
    },
    dateinputnormalcontainer: {
        height: '100%',
        width: '90%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        borderRadius: GetScreenHeight(1),
        borderWidth: 1,
        borderColor: '#323232',
        justifyContent: 'center',
        alignItems: 'center',
        transform: [{scale: 0.7}]
    },
    dateinputnormaltext: {
        color: '#323232',
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(45),
        transform: [{scale: 1.2}]
    },
    dateinputalternativecontainer: {
        height: '100%',
        width: '90%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    dateinputalternativearrowcontainer: {
        width: '30%',
        height: '100%',
        position: 'absolute',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    arrow: {
        tintColor: 'white',
        height: '100%',
        width: '100%',
        transform: [{scale: 0.65}]
    },
    dateinputalternativeline: {
        height: '100%',
        width: GetScreenWidth(0.5),
        position: 'absolute',
        left: '29%',
        top: '0%',
        backgroundColor: 'white',
    },
    dateinputalternativetext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(45),
    },
    dateinputalternativetextcontainer: {
        height: '100%', 
        width: '40%',
        position: 'absolute',
        left: '30%',
        top: '0%',
    },
    datetimepickertouch: {
        height: '100%',
        width: '40%',
        position: 'absolute',
        left: '30%',
        top: '0%',
    },
    leavetypecontainer: {
        height: '15%',
        width: '90%',
        position: 'absolute',
        left: '5%',
        top: '55%',
        borderRadius: GetScreenHeight(1),
        backgroundColor: '#323232',
        elevation: 2,
    },
    leavetypeheadingcontainer: {
        height: '90%',
        width: '45%',
        position: 'absolute',
        left: '5%',
        top: '5%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    leavetypeheading: {
        color: 'white',
        fontSize: DynamicFont(60),
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute',
        left: '0%',
    },
    leavetypeselectedcontainer: {
        height: '60%',
        width: '35%',
        position: 'absolute',
        right: '10%',
        top: '20%',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: GetScreenHeight(0.5),
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    leavetypeselectedcover: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: 'white',
    },
    leavetypeselectedtext: {
        color: 'white',
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(40),
        position: 'absolute',
    },
    leavetypedropdowncontainer: {
        height: GetScreenHeight(12),
        width:' 35%',
        position: 'absolute',
        right: '10%',
        top: '80%',
        borderBottomRightRadius: GetScreenHeight(1),
        borderBottomLeftRadius: GetScreenHeight(1),
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#323232'
    },
    leavetypedropdowncontent: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    timeoutcontainer: {
        height: '22.5%',
        width: '42.5%',
        position: 'absolute',
        left: '5%',
        top: '75%',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },  
    timeincontainer: {
        height: '22.5%',
        width: '42.5%',
        position: 'absolute',
        left: '52.5%',
        top: '75%',
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    timebuttonscontainer: {
        height: '75%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '25%',
    },
    coloncontainer: {
        height: '100%',
        width: '5%',
        position: 'absolute',
        left: '30.5%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    colon: {
        color: 'white',
        fontFamily: 'OpenSans-Bold',
        fontSize: DynamicFont(50),
    },
    timebuttonscolumn: {
        height: '100%',
        width: '33%',
        position: 'absolute',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    timebuttonarrowcontainer: {
        height: '30%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    timearrow: {
        height: '100%',
        width: '100%',
    },
    timebuttontimecontainer: {
        height: '35%',
        width: '70%',
        position: 'absolute',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: GetScreenHeight(0.5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    timebuttontimetext: {
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(40)
    },
    timeheadercontainer: {
        height: '25%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    timeheader: {
        fontFamily: 'Montserrat-Bold',
        fontSize: DynamicFont(45),
    },
    datetimepickermodal: {
        height: GetScreenHeight(110),
        width: GetScreenWidth(100),
        position: 'absolute',
        left: '0%',
        top: '-50%',
        backgroundColor: 'white',
        opacity: 0.75,
        elevation: 5,
    },
    datetimepickercontainer: {
        height: '55%',
        width: '90%',
        position: 'absolute',
        left: '5%',
        bottom: '0%',
        backgroundColor: '#323232',
        borderTopLeftRadius: GetScreenHeight(5),
        borderTopRightRadius: GetScreenHeight(5),
        elevation: 5,
    },
    dateitemscontainer: {
        height: '90%',
        width: '90%',
        position:'absolute',
        left: '7.5%',
        top: '10%',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    dateitem: {
        height: GetScreenHeight(6),
        width: GetScreenHeight(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 500,
    },
    dateitemtext: {
        color: 'white',
        fontSize: DynamicFont(50),
        fontFamily: 'OpenSans-Bold'
    },
    footer: {
        height: '15%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        bottom: '0%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    bottomline: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        bottom: '0%',
        backgroundColor: '#323232',
    },
    sendbuttoncontainer: {
        height: '70%',
        width: '25%',
        position: 'absolute',
        top: '15%',
        backgroundColor: '#4fc3f7',
        borderRadius: 500,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sendbuttontext: {
        fontSize: DynamicFont(40),
        fontFamily: 'OpenSans-Bold',
        color: 'white',
        marginLeft: GetScreenWidth(1)
    },
    sendbuttonimage: {
        tintColor: 'white',
        height: GetScreenHeight(3),
        width: GetScreenHeight(3),
        marginRight: GetScreenWidth(1)
    },
    leaveinbalancecontainer: {
        height: '90%',
        width: '30%',
        position: 'absolute',
        left: '35%',
        top: '5%',
    },
    leaveinbalanceheadercontainer: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    leaveinbalanceamountcontainer: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    leaveinbalancetext: {
        color: 'white',
        fontSize: DynamicFont(40),
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute'
    }
})