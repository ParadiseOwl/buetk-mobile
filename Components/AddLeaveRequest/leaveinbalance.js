import React, {Component} from "react";
import {View, StyleSheet, Text} from "react-native";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "../ResponsiveFont";
import axios from "axios";

export default class LeaveInBalance extends Component {
    constructor(){
        super();
        this.state = {
            leaveInBalance: 0
        }
    }

    GetLeaveInBalance(day, month, year){
        let data;
        axios.post('http://dev-buetk-backend.softperts.com/api/LeaveRequest/GetLeaveBalanceOnDate', {
            ToDate: {
                year: year,
                month: month,
                day: day,
            }
        }, {
            headers: {
                Authorization: 'Bearer ' + this.props.loginData.access_token
            }
        }).then(response => {
            data = response.data;
            if(typeof data !== 'object'){
                this.setState({
                    leaveInBalance: data
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }
    render(){
        return(
            <View style = {styles.container}>
                <Text style = {styles.leaveinbalancetext}>{this.state.leaveInBalance}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    leaveinbalancetext: {
        color: 'white',
        fontSize: DynamicFont(40),
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute',
        top: '5%'
    }
})