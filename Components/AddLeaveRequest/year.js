import React, {Component} from "react";
import {View, StyleSheet, Image, ImageBackground, Text} from "react-native";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "../ResponsiveFont";

export default class Year extends Component {
    constructor(){
        super();
        this.state = {
            year: 2020,
        }
    }

    GetValue(){
        return this.state.year;
    }

    SetValue(year){
        this.setState({year});
    }

    render(){
        return(
            <View style = {styles.container}>
                <Text style = {[styles.yeartext, {color: this.props.whiteversion ? '#323232' : 'white'}]}>{this.state.year}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    yeartext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(45),
    }
})