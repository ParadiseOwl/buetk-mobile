import React, {Component} from "react";
import {View, StyleSheet, Image, Text, ImageBackground, Animated, Easing} from "react-native";
import {DynamicFont, GetScreenHeight, GetScreenWidth} from "../ResponsiveFont";

export default class Month extends Component {
    constructor(){
        super();
        this.state = {
            month: 1,
            monthName: 'Jan',
        }
    }

    GetMonthValue(){
        return this.state.month;
    }

    SetMonthValue(month, monthName){
        this.setState({
            month,
            monthName
        })
    }

    render(){
        return(
            <View style = {styles.container}>
                <Text style = {[styles.monthtext, {color: this.props.whiteversion ? '#323232' : 'white'}]}>{this.state.monthName}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    monthtext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(45),
    }
})