import React, {Component} from "react";
import {View, StyleSheet, Image, Text, ImageBackground, Animated, Easing} from "react-native";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "../ResponsiveFont";

export default class Day extends Component {
    constructor(){
        super();
        this.state = {
            day: 1
        }
    }

    SetValue(value){
        this.setState({
            day: value
        })
    }

    GetValue(){
        return this.state.day;
    }

    render(){
        return(
            <View style = {styles.container}>
                <Text style = {[styles.daytext, {color: this.props.whiteversion ? '#323232' : 'white'}]}>{this.state.day}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    daytext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(45),
    }
})