import React, {Component} from "react";
import {View, StyleSheet, Image, Text} from "react-native";
import {GetScreenWidth, GetScreenHeight, DynamicFont} from "../ResponsiveFont";

export default class Hour extends Component {
    constructor(){
        super()
        this.state = {
            hour: 1
        }
    }

    GetValue(){
        return this.state.hour;
    }

    SetValue(hour){
        this.setState({
            hour
        })
    }

    render(){
        return(
            <View style = {styles.container}>
                <Text style = {[styles.hourtext, {color: this.props.color}]}>{this.state.hour}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    hourtext: {
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(40)
    }
})