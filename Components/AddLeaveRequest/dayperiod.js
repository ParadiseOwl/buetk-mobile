import React, {Component} from "react";
import {View, StyleSheet, Animated, Text, Easing} from "react-native";
import {GetScreenWidth, GetScreenHeight, DynamicFont} from "../ResponsiveFont";

export default class DayPeriod extends Component {
    constructor(){
        super();
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            animateDayPeriod: new Animated.Value(0),
            dayPeriod: false,
        }
    }

    ChangeValue(){
        let dayPeriod = this.state.dayPeriod;
        dayPeriod = !dayPeriod;
        Animated.timing(this.state.animateDayPeriod, {
            toValue: 100,
            duration: 400,
            easing: this.state.easing,
            useNativeDriver: true
        }).start(
            ()=> {
                this.setState({
                    dayPeriod
                });
                Animated.timing(this.state.animateDayPeriod, {
                    toValue: 0,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true
                }).start();
            }
        );
    }

    render(){
        return(
            <View style = {styles.container}>
                <Animated.Text style = {[styles.daytext, {color: this.props.color, opacity: this.state.animateDayPeriod.interpolate({
                    inputRange: [0, 100],
                    outputRange: [1, 0]
                }), transform: [{translateY: this.state.animateDayPeriod.interpolate({
                    inputRange: [0, 100],
                    outputRange: [0, -10]
                })}]}]}>{this.state.dayPeriod ? 'AM' : 'PM'}</Animated.Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    daytext: {
        fontFamily: 'OpenSans-Bold',
        fontSize: DynamicFont(40),
        letterSpacing: GetScreenHeight(0.1)
        
    }
})