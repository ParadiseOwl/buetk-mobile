import React, {Component} from "react";
import {View, StyleSheet, Image, Text} from "react-native";
import {GetScreenWidth, GetScreenHeight, DynamicFont} from "../ResponsiveFont";

export default class Minute extends Component {
    constructor(){
        super();
        this.state = {
            minute: 0
        }
    }

    GetValue(){
        return this.state.minute;
    }

    SetValue(minute){
        this.setState({
            minute
        });
    }
    
    render(){
        return(
            <View style = {styles.container}>
                <Text style = {[styles.minutetext, {color: this.props.color}]}>{this.state.minute}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    minutetext: {
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(40)
    }
})