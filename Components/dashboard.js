import React, {Component} from "react";
import {View, StyleSheet, ImageBackground, Text, TextInput, Animated, Easing, TouchableWithoutFeedback, Dimensions, Image} from "react-native";
import {DynamicFont, GetScreenHeight, GetScreenWidth} from "./ResponsiveFont";
import axios from "axios";

//imported pages
import DashboardMain from "./dashboardmain";
import Payroll from "./payroll";
import AsyncStorage from "@react-native-community/async-storage";

export default class Dashboard extends Component {
    constructor(){
        super()
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateTextOne: new Animated.Value(0),
            animateTextTwo: new Animated.Value(0),
            animateTextThree: new Animated.Value(0),
            animateTextFour: new Animated.Value(0),
            animateTextFive: new Animated.Value(0),
            animateTextSix: new Animated.Value(0),
            animateTextSeven: new Animated.Value(0),
            animateDot: new Animated.Value(0),
            animateUsername: new Animated.Value(0),
            animateMenuIcon: new Animated.Value(0),
            animateHeader: new Animated.Value(0),
            animatePayroll: new Animated.Value(0),
            animateBoxOne: new Animated.Value(0),
            animateBoxTwo: new Animated.Value(0),
            animateBoxThree: new Animated.Value(0),
            animateBoxfour: new Animated.Value(0),
            animateSidePanel: new Animated.Value(0),
            animateMenuBar: new Animated.Value(0),
            animatePage: new Animated.Value(0),
            animateMenu: new Animated.Value(0),
            animateContainerShell: new Animated.Value(0),

            //standard variables
            menuActive: false,
            pageActive: 0,
            loginData: [],
            userData: [],
        }
        this.ChangePageToPayroll = this.ChangePageToPayroll.bind(this);
        this.ChangePageToHome = this.ChangePageToHome.bind(this);
    }

    async componentDidMount(){
        this.AnimateWelcomeText();
        let loginData = await AsyncStorage.getItem('user-data');
        loginData = JSON.parse(loginData);
        this.setState({
            loginData
        });
        this.GetUserDetails(loginData);
    }

    GetUserDetails(data){
        let userData = [];
        axios.post('http://dev-buetk-backend.softperts.com/api/employee/GetUserDetailsByUserName', {
            userName: data.userName,
        },{
            headers: {
                Authorization: 'Bearer ' + data.access_token
            }
        }).then(response => {
            userData = response.data;
            this.setState({
                userData
            })
        }).catch(error => {
            console.log(error)
        })
    }

    //Animated functions here
    AnimateWelcomeText(){
        Animated.stagger(100,[
            Animated.timing(this.state.animateTextOne, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextThree, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextTwo, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextFour, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextSeven, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextSix, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextFive, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start(()=> {
            this.AnimateWelcomeTextAlt();
            this.AnimateDot();
        });
    }

    AnimateWelcomeTextAlt(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateTextOne, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextTwo, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextThree, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextFour, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextFive, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextSix, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateTextSeven, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start();
    }
    
    AnimateDot(){
        Animated.timing(this.state.animateDot, {
            toValue: 100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true
        }).start(()=> {
            this.AnimateIntro();
        });
    }

    AnimateIntro(){
        Animated.stagger(100, [
            Animated.parallel([
                Animated.timing(this.state.animateUsername, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMenuIcon, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing, 
                    useNativeDriver: true
                })
            ]),
        ]).start(()=> {
            this.refs.dashboard.AnimateShow();
        });
    }

    ChangePageToPayroll(){
        setTimeout(() => {
            this.setState({
                pageActive: 1,
            })
            this.refs.payroll.AnimateShow();
        }, 1500);
    }
    
    ChangePageToHome(){
        setTimeout(() => {
            this.setState({
                pageActive: 0,
            })
            this.refs.dashboard.AnimateShow();
        }, 800);
    }

    AnimateToMenu(){
        Animated.stagger(100, [
            Animated.parallel([
                Animated.timing(this.state.animatePage, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true
                }),
                Animated.timing(this.state.animateMenu,{
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true
                }),
                Animated.timing(this.state.animateContainerShell, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateMenuBar, {
                toValue: 100,
                duration: 800,
                delay: 600,
                easing: this.state.easing,
                useNativeDriver: true
            })
        ]).start();
    }

    AnimateToDashboard(){
        Animated.stagger(100, [
            Animated.parallel([
                Animated.timing(this.state.animatePage, {
                    toValue: 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMenu, {
                    toValue: 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true
                }),
                Animated.timing(this.state.animateContainerShell, {
                    toValue: 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateMenuBar, {
                toValue: 0,
                duration: 800,
                delay: 750,
                easing: this.state.easing,
                useNativeDriver: true
            })
        ]).start();
    }

    CloseMenu(){
        this.AnimateToDashboard();
    }

    OpenMenu(){
        this.AnimateToMenu();
    }

    OpenHomeFromMenu(){
        this.refs.payroll.AnimateHide();
        setTimeout(() => {
            this.setState({
                pageActive: 0,
            })
            this.refs.dashboard.AnimateShow();
        }, 800);
    }

    OpenPayrollFromMenu(){
        this.refs.dashboard.AnimateHide();
        setTimeout(() => {
            this.setState({
                pageActive: 1,
            })
            this.refs.payroll.AnimateShow();
        }, 800);
    }
    
    render(){
        return(
            <>
                <Animated.View style = {[styles.menucontainer, {}]}>
                    <Animated.View style = {[styles.menucontainer, {backgroundColor: 'white'}]} />
                    <Animated.View style = {[styles.menucontainer, {backgroundColor: '#323232', opacity: this.state.animateMenu.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    })}]} />
                    <TouchableWithoutFeedback onPress = {this.CloseMenu.bind(this)}>
                        <View style = {styles.menuiconcontainer}>
                            <Animated.View style = {[styles.menuiconbarfour, {opacity: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, 1]
                            }), transform: [{translateX: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [-10, 0]
                            })}]}]} />
                            <Animated.View style = {[styles.menuiconbarfive, {opacity: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, 1]
                            }), transform: [{translateX: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [10, 0]
                            })}]}]} />
                        </View>
                    </TouchableWithoutFeedback>
                    <View style = {styles.menuheadercontainer}>
                        <TouchableWithoutFeedback onPress = {this.OpenHomeFromMenu.bind(this)}>
                            <View style = {styles.menuheadersubcontainer}>
                                <Text style = {styles.menuheader}>Home</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.OpenPayrollFromMenu.bind(this)}>
                            <View style = {styles.menuheadersubcontainer}>
                                <Text style = {styles.menuheader}>Payroll</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style = {styles.menuheadersubcontainer}>
                            <Text style = {styles.menuheader}>Accounts</Text>
                        </View>
                        <View style = {styles.menuheadersubcontainer}>
                            <Text style = {styles.menuheader}>Inventory</Text>
                        </View>
                        <View style = {styles.menuheadersubcontainer}>
                            <Text style = {styles.menuheader}>Contact Center</Text>
                        </View>
                    </View>
                    <View style = {styles.logoutcontainer}>
                        <Image style = {styles.logout} resizeMode = "contain" source = {require('./resources/power.png')}/>
                        <Text style = {styles.logouttext}>Logout</Text>
                    </View>
                </Animated.View>
                <Animated.View style = {[styles.container, {transform: [{scaleX: this.state.animatePage.interpolate({inputRange: [0, 100], outputRange: [1, 0.75]})}, {scaleY: this.state.animatePage.interpolate({inputRange: [0, 100], outputRange: [1, 0.75]})}, {translateX: this.state.animatePage.interpolate({inputRange: [0, 100], outputRange: [0, GetScreenWidth(65)]})}]}]}>
                    <View style = {[styles.viewport, ]}>
                        <Animated.View style = {[styles.menuiconcontainer, {opacity: this.state.animateMenuIcon.interpolate({
                            inputRange: [0, 100],
                            outputRange: [0, 1]
                        }), transform: [{translateX: this.state.animateMenuIcon.interpolate({
                            inputRange: [0, 100],
                            outputRange: [-10, 0]
                        })}]}]}>
                            <Animated.View style = {[styles.menuiconbarone, {opacity: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            })}]} />
                            <Animated.View style = {[styles.menuiconbartwo, {opacity: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            })}]} />
                            <Animated.View style = {[styles.menuiconbarthree, {opacity: this.state.animateMenuBar.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            })}]} />
                        </Animated.View>
                        <TouchableWithoutFeedback onPress = {this.OpenMenu.bind(this)}>
                            <View style = {styles.menuiconpresscontainer} />
                        </TouchableWithoutFeedback>
                        {this.state.pageActive === 0 && (
                            <DashboardMain ref = "dashboard" ChangePageToPayroll = {this.ChangePageToPayroll} />
                        )}
                        {this.state.pageActive === 1 && (
                            <Payroll userData = {this.state.userData} loginData = {this.state.loginData} ref = "payroll" ChangePageToHome = {this.ChangePageToHome} />
                        )}
                    </View>
                    <Animated.View style = {[styles.usernamecontainer, {opacity: this.state.animateUsername.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateX: this.state.animateUsername.interpolate({
                        inputRange: [0, 100],
                        outputRange: [10, 0]
                    })}]}]}>
                        <Text style = {styles.username}>{this.state.loginData.userName}</Text>
                    </Animated.View>
                    <Animated.View style = {[styles.dot, {transform: [{translateX: this.state.animateDot.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, GetScreenWidth(42.5)]
                    })}, {translateY: this.state.animateDot.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, -GetScreenHeight(42.5)]
                    })}]}]} />
                    <View style = {styles.welcometextcontainer}>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextOne.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextOne.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>W</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextTwo.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextTwo.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>E</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextThree.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextThree.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>L</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextFour.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextFour.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>C</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextFive.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextFive.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>O</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextSix.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextSix.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>M</Animated.Text>
                        <Animated.Text style = {[styles.welcometext, {opacity: this.state.animateTextSeven.interpolate({inputRange: [0, 100], outputRange: [0, 1]}), transform: [{translateY: this.state.animateTextSeven.interpolate({inputRange: [0, 100], outputRange: [10, 0]})}]}]}>E</Animated.Text>
                    </View>
                </Animated.View>
                <TouchableWithoutFeedback onPress = {this.CloseMenu.bind(this)}>
                    <Animated.View style = {[styles.container, {backgroundColor: 'transparent', transform: [{scale: 0.75}, {translateX: this.state.animateContainerShell.interpolate({
                        inputRange: [0, 100],
                        outputRange: [GetScreenWidth(120), GetScreenWidth(65)]
                    })}]}]} /> 
                </TouchableWithoutFeedback>
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    menuiconcontainer: {
        height: '3.25%',
        width: GetScreenHeight(3.25),
        position: 'absolute',
        left: '5%',
        top: '3%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuiconbarone: {
        height: GetScreenHeight(0.35),
        width: '100%',
        borderRadius: 500,
        backgroundColor: '#323232',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    menuiconbartwo: {
        height: GetScreenHeight(0.35),
        width: '75%',
        borderRadius: 500,
        backgroundColor: '#323232',
        position: 'absolute',
        left: '0%',
    },
    menuiconbarthree: {
        height: GetScreenHeight(0.35),
        width: '50%',
        borderRadius: 500,
        backgroundColor: '#323232',
        position: 'absolute',
        left: '0%',
        bottom: '0%',
    },
    menuiconbarfour: {
        height: GetScreenHeight(0.35),
        width: '100%',
        borderRadius: 500,
        backgroundColor: 'white',
        bottom: '10%',
    },
    menuiconbarfive: {
        height: GetScreenHeight(0.35),
        width: '100%',
        borderRadius: 500,
        backgroundColor: 'white',
        top: '10%',
    },
    menuiconpresscontainer: {
        height: '5%',
        width: GetScreenHeight(5),
        position: 'absolute',
        left: '5%',
        top: '3%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    usernamecontainer: {
        position: 'absolute',
        top: '5%',
        right: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    username: {
        color: '#323232',
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(50)
    },
    dot: {
        height: GetScreenHeight(5),
        width: GetScreenHeight(5),
        position: 'absolute',
        backgroundColor: '#323232',
        borderRadius: 500,
    },
    welcometextcontainer: {
        flexDirection: 'row',
        top: '10%',
    },
    welcometext: {
        color: '#323232',
        fontFamily: 'Montserrat-Regular',
        letterSpacing: 2,
    },
    menucontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    menuheadercontainer: {
        height: '65%',
        width: '45%',
        position: 'absolute',
        left: '5%',
        top: '20%',
    },
    menuheadersubcontainer: {
        height: '20%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuheader: {
        color: 'white',
        fontFamily: 'Montserrat-Bold',
        fontSize: DynamicFont(50),
        position: 'absolute',
        left: '10%',
    },
    logoutcontainer: {
        height: '5%',
        position: 'absolute',
        right: '5%',
        top: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: GetScreenHeight(0.5),
        flexDirection: 'row',
        padding: GetScreenHeight(1)
    },
    logout: {
        height: '50%',
        width: GetScreenHeight(2.5),
        tintColor: 'white'
    },
    logouttext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(35)

    }
})