import React, {Component, Fragment} from "react";
import {Dimensions, PixelRatio, Alert} from "react-native";

const screenwidth = Dimensions.get("screen").width;
const screenwidthfixed = 411;

export function FontSize(per){
    if(screenwidthfixed < screenwidth){ 
        return ((screenwidth/100)* per);
    }
    else {
        //scale factor
        return (((screenwidth/100)* per)* 0.75);
    }
}

//keeping 1080p as our base resolution
export function DynamicFont(size){
    const PR = PixelRatio.get();
    var output = (size/PR);
    //now we compare the resolutions by multiplying the pixel density and width
    const screenwidth = Dimensions.get("screen").width;
    if(PR > 2){
        output = (output/(1080/(screenwidth * PR)));
        const negate = (output/100) * 16;
        output = output - negate;
        return output;
    }
    else{
        output = (output/(1080/(screenwidth * PR)));
        const negate = (output/100) * 16; //15
        output = output - negate;
        return output;
    }
}

export function GetScreenWidth(per){
    const onepercent = Dimensions.get("screen").width/100;
    return (onepercent * per);
}

export function GetScreenHeight(per){
    const onepercent = Dimensions.get("screen").height/100;
    return (onepercent * per);
}
