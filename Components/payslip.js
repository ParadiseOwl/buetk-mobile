import React, {Component, createRef} from "react";
import {View, StyleSheet, Text, Image, ImageBackground, Animated, Easing, TouchableWithoutFeedback, PanResponder, ScrollView} from "react-native";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "./ResponsiveFont";

export default class Payslip extends Component {
    constructor(){
        super()
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateHeader: new Animated.Value(0),
            animateBody: new Animated.Value(0),
            animateFooter: new Animated.Value(0),
            animateHeaderText: new Animated.Value(1),
            swiper: new Animated.Value(0),
            animateExtraInformation: new Animated.Value(0),

            //standard variables
            employeecategories: ['Employee', 'CNIC', 'Designation', 'Basic Pay', 'Address', 'Cadre', 'Account', 'Department', 'Pay Scale'],
            headertext: 'Payslip for August, 2019',
            grossPay: 0,
            netDeduction: 0,
            incomeTaxDeduction: 0,
            loan: 0,
            netPay: 0,
            extrainformationdetail: 'Gross Pay',
            extrainformationamount: '150,107',
            monthName: 'January',
            year: 2020,
        }
        this.allowanceDetailsScroll = createRef();
        this.allowanceAmountScroll = createRef();

        let current = 0;
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderMove: (evt, gestureState) => {
                if(current !== 0 && gestureState.dx > 0){
                    this.state.swiper.setValue(gestureState.dx + (-GetScreenWidth(100) * current));
                }
                else if(current !== 3 && gestureState.dx < 0){
                    this.state.swiper.setValue(gestureState.dx + (-GetScreenWidth(100) * current));
                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                if(gestureState.dx < -GetScreenWidth(40) && current !== 3){
                    current = current + 1;
                }
                else if(gestureState.dx > GetScreenWidth(40) && current !== 0){
                    current = current - 1;
                }
                Animated.timing(this.state.swiper, {
                    toValue: -GetScreenWidth(100) * current,
                    duration: 400,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }).start();
                this.onPageChange(current);
            }
        })
    }
    
    componentDidMount(){
        this.OnPageOpenAnimation();
        this.GetExtraInformation();
        this.setState({
            monthName: this.GetMonth(this.props.payslipData.currentMonth),
            year: this.props.payslipData.currentYear,
            headertext: 'Payslip For ' + this.GetMonth(this.props.payslipData.currentMonth) + ', ' + this.props.payslipData.currentYear.toString(),
        })
    }

    GetExtraInformation(){
        let totalAllowance = 0;
        this.props.payslipData.allowances.forEach(item => {
            totalAllowance = totalAllowance + parseInt(item.amount);
        })
        let totalDeduction = 0, loan = 0, incomeTaxDeduction = 0;
        this.props.payslipData.deductions.forEach(item => {
            totalDeduction = totalDeduction + parseInt(item.amount);
            if(item.name === "Loan"){
                loan = item.amount;
            }
            if(item.name === "IncomeTax"){
                incomeTaxDeduction = item.amount;
            }
        })
        let total = parseInt(this.props.payslipData.basicPay) + totalAllowance - totalDeduction;
        let grossPay = parseInt(this.props.payslipData.basicPay) + totalAllowance;

        this.setState({
            grossPay,
            netDeduction: totalDeduction,
            incomeTaxDeduction,
            loan,
            netPay: total
        })
    }

    GetMonth(month){
        if(month === 1) {return 'January'}
        else if(month === 2) {return 'Febuary'}
        else if(month === 3) {return 'March'}
        else if(month === 4) {return 'April'}
        else if(month === 5) {return 'May'}
        else if(month === 6) {return 'June'}
        else if(month === 7) {return 'July'}
        else if(month === 8) {return 'August'}
        else if(month === 9) {return 'September'}
        else if(month === 10) {return 'October'}
        else if(month === 11) {return 'November'}
        else if(month === 12) {return 'December'}
    }

    onPageChange(current){
        if(current === 0){
            this.ChangeHeaderText('Payslip For ' + this.state.monthName + ', ' + this.state.year.toString());
            this.HideExtraInformation();
        }
        else if(current === 1){
            this.ChangeHeaderText('Basic Pay: ' + this.props.payslipData.basicPay.toString());
            if(this.state.extrainformationdetail === ""){
                this.ShowExtraInformation('Gross Pay', this.state.grossPay.toString())
            }
            else {
                this.ChangeExtraInformation('Gross Pay', this.state.grossPay.toString())
            }
        }
        else if(current === 2){
            this.ChangeHeaderText('Gross Pay: ' + this.state.grossPay.toString());
            if(this.state.extrainformationdetail === ""){
                this.ShowExtraInformation('Net Pay', this.state.netPay.toString())
            }
            else{
                this.ChangeExtraInformation('Net Pay', this.state.netPay.toString());
            }
        }
        else {
            this.ChangeHeaderText('Payslip For ' + this.state.monthName + ', ' + this.state.year.toString());
            this.HideExtraInformation();
        }
    }

    OnPageOpenAnimation(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBody, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFooter, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    OnPageCloseAnimation(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBody, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFooter, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start(()=> {
            this.props.AnimateShowPartial();
        });
    }

    ChangeHeaderText(text){
        Animated.timing(this.state.animateHeaderText, {
            toValue: 0,
            duration: 400,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start(()=> {
            this.setState({
                headertext: text
            });
            Animated.timing(this.state.animateHeaderText, {
                toValue: 1,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true 
            }).start();
        });
    }

    ShowExtraInformation(details, amount){
        this.setState({
            extrainformationdetail: details,
            extrainformationamount: amount
        });
        Animated.timing(this.state.animateExtraInformation, {
            toValue: 100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }

    HideExtraInformation(){
        this.setState({
            extrainformationdetail: "",
            extrainformationamount: "",
        })
        Animated.timing(this.state.animateExtraInformation, {
            toValue: 0,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }

    ChangeExtraInformation(details, amount){
        Animated.timing(this.state.animateExtraInformation, {
            toValue: 0,
            duration: 400,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start(() => {
            this.setState({
                extrainformationdetail: details,
            extrainformationamount: amount
            });
            Animated.timing(this.state.animateExtraInformation, {
                toValue: 100,
                duration: 400,
                easing: this.state.easing,
                useNativeDriver: true,
            }).start();
        });
    }

    render(){
        return(
            <View style = {styles.container}>
                <View style = {styles.viewport}>
                    <Animated.View style = {[styles.payslipheadercontainer, {opacity: this.state.animateHeader.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateY: this.state.animateHeader.interpolate({
                        inputRange: [0, 100],
                        outputRange: [-10, 0]
                    })}]}]}>
                        <Animated.Text style = {[styles.payslipheader, {opacity: this.state.animateHeaderText}]}>{this.state.headertext}</Animated.Text>
                    </Animated.View>
                    <Animated.View style = {[styles.scrollcontainer, {opacity: this.state.animateBody.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateX: this.state.animateBody.interpolate({
                        inputRange: [0, 100],
                        outputRange: [-10, 0]
                    })}]}]}>
                        <Animated.View style = {[styles.employeedatacontainer, {opacity: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [0, 0, 0, 1]
                        }), transform: [{translateX: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [-GetScreenWidth(75), -GetScreenWidth(75), -GetScreenWidth(75), 0]
                        })}, {rotateY: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: ['50deg', '50deg', '50deg', '0deg']
                        })}]}]}>
                            <View style = {styles.employeecontainer}>
                                {this.state.employeecategories.map(item => (
                                    <Text style = {[styles.employee, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{item}</Text>
                                ))}
                            </View>
                            <View style = {styles.employeedetailscontainer}>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.employeeName}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.cnic}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.designation}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.basicPay}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.address}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.cadre}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.account + this.props.payslipData.bank}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.department}</Text>
                            <Text style = {[styles.employeedetails, {height: (90/this.state.employeecategories.length).toString() + "%"}]}>{this.props.payslipData.payScale}</Text>
                            </View>
                        </Animated.View>
                        <Animated.View style = {[styles.allowancedatacontainer, {opacity: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [0, 0, 1, 0]
                        }), transform: [{translateX: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [-GetScreenWidth(75), -GetScreenWidth(75), 0, GetScreenWidth(75)]
                        })}, {rotateY: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: ['-50deg', '-50deg', '0deg', '50deg']
                        })}]}]}>
                            <View style = {styles.allowancedetailscontainer}>
                                <View style = {styles.allowanceheadercontainer}>
                                    <Text style = {styles.allowanceheader}>Allowance</Text>
                                </View>
                                <View style = {styles.allowancedetailsscrollcontainer}>
                                    <ScrollView style = {styles.allowancedetailsscroll} ref = {ref => this.allowanceAmountScroll = ref} onScroll = {event => {
                                        this.allowanceDetailsScroll.scrollTo({x: 0, y: event.nativeEvent.contentOffset.y});
                                    }} >
                                        {this.props.payslipData.allowances.map(item => (
                                            <View style = {styles.allowancedetails}>
                                                <Text style = {styles.allowancedetailstext}>{item.name}</Text>
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>
                            </View>
                            <View style = {styles.allowanceamountcontainer}>
                                <View style = {styles.allowanceamountheadercontainer}>
                                    <Text style = {styles.allowanceamountheader}>Amount</Text>
                                </View>
                                <View style = {styles.allowanceamountscrollcontainer}>
                                    <ScrollView style = {styles.allowanceamountscroll} ref = {ref => this.allowanceDetailsScroll = ref} onScroll = {event => {
                                        this.allowanceAmountScroll.scrollTo({x: 0, y: event.nativeEvent.contentOffset.y});
                                    }} >
                                        {this.props.payslipData.allowances.map(item => (
                                            <View style = {styles.allowanceamount}>
                                                <Text style = {styles.allowanceamounttext}>{item.amount}</Text>
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>
                            </View>
                        </Animated.View>
                        <Animated.View style = {[styles.deductiondatacontainer, {opacity: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [0, 1, 0, 0]
                        }), transform: [{translateX: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [-GetScreenWidth(75), 0, GetScreenWidth(75), GetScreenWidth(75)]
                        })}, {rotateY: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: ['-50deg', '0deg', '50deg', '50deg']
                        })}]}]}>
                            <View style = {styles.deductiondetailscontainer}>
                                <View style = {styles.deductionheadercontainer}>
                                    <Text style = {styles.deductionheader}>Deduction</Text>
                                </View>              
                                <View style = {styles.deductiondetailsscrollcontainer}>
                                    <ScrollView style = {styles.deductiondetailsscroll}>
                                        {this.props.payslipData.deductions.map(item => (
                                            <View style = {styles.deductiondetails}>
                                                <Text style = {styles.deductiondetailstext}>{item.name}</Text>
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>
                            </View>
                            <View style = {styles.deductionamountcontainer}>
                                <View style = {styles.deductionamountheadercontainer}>
                                    <Text style = {styles.deductionamountheader}>Amount</Text>
                                </View>
                                <View style = {styles.deductionamountscrollcontainer}>
                                    <ScrollView style = {styles.deductionamountscroll}>
                                        {this.props.payslipData.deductions.map(item => (
                                            <View style = {styles.deductionamount}>
                                                <Text style = {styles.deductionamounttext}>{item.amount}</Text>
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>
                            </View>
                        </Animated.View>
                        <Animated.View style = {[styles.summarydatacontainer, {opacity: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [1, 0, 0, 0]
                        }), transform: [{translateX: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: [0, GetScreenWidth(75), GetScreenWidth(75), GetScreenWidth(75)]
                        })}, {rotateY: this.state.swiper.interpolate({
                            inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                            outputRange: ['0deg', '-50deg', '-50deg', '-50deg']
                        })}]}]}>
                            <View style = {styles.summaryheadercontainer}>
                                <View style = {styles.summaryheadersubcontainer}>
                                    <Text style = {styles.summaryheader}>Gross Pay</Text>
                                </View>
                                <View style = {styles.summaryheadersubcontainer}>
                                    <Text style = {styles.summaryheader}>Net Deduction</Text>
                                </View>
                                <View style = {styles.summaryheadersubcontainer}>
                                    <Text style = {styles.summaryheader}>Income Tax Deduction</Text>
                                </View>
                                <View style = {styles.summaryheadersubcontainer}>
                                    <Text style = {styles.summaryheader}>Loan</Text>
                                </View>
                                <View style = {styles.summaryheadersubcontainer}>
                                    <Text style = {styles.summaryheader}>Net Pay</Text>
                                </View>
                            </View>
                            <View style = {styles.summaryamountcontainer}>
                                <View style = {styles.summaryamountheadercontainer}>
                                    <Text style = {styles.summaryamountheader}>{this.state.grossPay}</Text>
                                </View>
                                <View style = {styles.summaryamountheadercontainer}>
                                    <Text style = {styles.summaryamountheader}>{this.state.netDeduction}</Text>
                                </View>
                                <View style = {styles.summaryamountheadercontainer}>
                                    <Text style = {styles.summaryamountheader}>{this.state.incomeTaxDeduction}</Text>
                                </View>
                                <View style = {styles.summaryamountheadercontainer}>
                                    <Text style = {styles.summaryamountheader}>{this.state.loan}</Text>
                                </View>
                                <View style = {styles.summaryamountheadercontainer}>
                                    <Text style = {styles.summaryamountheader}>{this.state.netPay}</Text>
                                </View>
                            </View>
                        </Animated.View>
                    </Animated.View>
                    <View style = {styles.panhandlercontainer} {...this._panResponder.panHandlers} />
                    <Animated.View style = {[styles.payslipfooter, {opacity: this.state.animateFooter.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateY: this.state.animateFooter.interpolate({
                        inputRange: [0, 100],
                        outputRange: [10, 0]
                    })}]}]}>
                        <View style = {styles.extrainformationcontainer}>
                            <Animated.View style = {[styles.extrainformationtopheader, {opacity: this.state.animateExtraInformation.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, 1]
                            }), transform: [{translateX: this.state.animateExtraInformation.interpolate({
                                inputRange: [0, 100],
                                outputRange: [-15, 0]
                            })}]}]}>
                                <Text style = {styles.extrainformationheader}>{this.state.extrainformationdetail}</Text>
                            </Animated.View>
                            <Animated.View style = {[styles.extrainformationbottomheader, {opacity: this.state.animateExtraInformation.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, 1]
                            }), transform: [{translateX: this.state.animateExtraInformation.interpolate({
                                inputRange: [0, 100],
                                outputRange: [15, 0]
                            })}]}]}>
                                <Text style = {styles.extrainformationheader}>{this.state.extrainformationamount}</Text>
                            </Animated.View>
                        </View>
                        <View style = {styles.dotscontainer}>
                            <View style = {[styles.dot, {borderWidth: 1, backgroundColor: 'transparent', borderColor: '#323232'}]} />
                            <View style = {[styles.dot, {borderWidth: 1, backgroundColor: 'transparent', borderColor: '#323232'}]} />
                            <View style = {[styles.dot, {borderWidth: 1, backgroundColor: 'transparent', borderColor: '#323232'}]} />
                            <View style = {[styles.dot, {borderWidth: 1, backgroundColor: 'transparent', borderColor: '#323232'}]} />
                        </View>
                        <View style = {styles.dotscontainer}>
                            <Animated.View style = {[styles.dot, {opacity: this.state.swiper.interpolate({
                                inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                                outputRange: [0, 0, 0, 1]
                            })}]} />
                            <Animated.View style = {[styles.dot, {opacity: this.state.swiper.interpolate({
                                inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                                outputRange: [0, 0, 1, 0]
                            })}]} />
                            <Animated.View style = {[styles.dot, {opacity: this.state.swiper.interpolate({
                                inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                                outputRange: [0, 1, 0, 0]
                            })}]} />
                            <Animated.View style = {[styles.dot, {opacity: this.state.swiper.interpolate({
                                inputRange: [-GetScreenWidth(100) * 3, -GetScreenWidth(100) * 2, -GetScreenWidth(100), 0],
                                outputRange: [1, 0, 0, 0]
                            })}]} />
                        </View>
                        <TouchableWithoutFeedback onPress = {this.OnPageCloseAnimation.bind(this)}>
                            <View style = {styles.backbuttoncontainer}>
                                <View style = {styles.backbutton}>
                                    <Text style = {styles.backbuttontext}>Back</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '70%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '30%',
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    payslipheadercontainer: {
        position: 'absolute',
        top: '0%',
        left: '5%',
    },
    payslipheader: {
        color: '#323232',
        fontFamily: 'Nunito-ExtraBold',
        fontSize: DynamicFont(75)
    },
    scrollcontainer: {
        height: '70%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '12.5%',
    },
    employeedatacontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    employeecontainer: {
        height: '100%',
        width: '40%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        elevation: 2,
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    employee: {
        color: '#323232',
        fontFamily: 'OpenSans-Bold',
        fontSize: DynamicFont(40)
    },
    employeedetailscontainer: {
        height: '100%',
        width: '45%',
        position: 'absolute',
        left: '50%',
        top: '0%',
        elevation: 2,
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
        justifyContent: 'center',
        alignItems: 'center',
    },
    employeedetails: {
        color: 'white',
        fontFamily: 'OpenSans-Regular',
        fontSize: DynamicFont(40)
    },
    allowancedatacontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    allowancedetailscontainer: {
        height: '100%',
        width: '55%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    allowanceheadercontainer: {
        height: '10%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    allowanceheader: {
        color: '#323232',
        fontFamily: 'Montserrat-Bold',
    },
    allowancedetailsscrollcontainer: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '10%',
    },
    allowancedetailsscroll: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    allowancedetails: {
        height: GetScreenHeight(3),
        width: GetScreenWidth(55),
        justifyContent: 'center',
        alignItems: 'center',
    },
    allowancedetailstext: {
        color: '#323232',
        fontFamily: 'Roboto-Regular'
    },
    allowanceamountcontainer: {
        height: '100%',
        width: '32.5%',
        position: 'absolute',
        right: '5%',
        top: '0%',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    allowanceamountheadercontainer: {
        height: '10%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    allowanceamountheader: {
        color: 'white',
        fontFamily: 'Montserrat-Bold',
    },
    allowanceamountscrollcontainer: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '10%',
    },
    allowanceamountscroll: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    allowanceamount: {
        height: GetScreenHeight(3),
        width: GetScreenWidth(32.5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    allowanceamounttext: {
        color: 'white',
        fontFamily: 'Roboto-Regular'
    },
    deductiondatacontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    deductiondetailscontainer: {
        height: '100%',
        width: '55%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    deductionheadercontainer: {
        height: '10%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    deductionheader: {
        color: 'white',
        fontFamily: 'Montserrat-Bold',
    },
    deductiondetailsscrollcontainer: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '10%',
    },
    deductiondetailsscroll: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    deductiondetails: {
        height: GetScreenHeight(3),
        width: GetScreenWidth(55),
        justifyContent: 'center',
        alignItems: 'center',
    },
    deductiondetailstext: {
        color: 'white',
        fontFamily: 'Roboto-Regular'
    },
    deductionamountcontainer: {
        height: '100%',
        width: '32.5%',
        position: 'absolute',
        right: '5%',
        top: '0%',
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    deductionamountheadercontainer: {
        height: '10%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    deductionamountheader: {
        color: '#323232',
        fontFamily: 'Montserrat-Bold',
    },
    deductionamountscrollcontainer: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '10%',
    },
    deductionamountscroll: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    deductionamount: {
        height: GetScreenHeight(3),
        width: GetScreenWidth(32.5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    deductionamounttext: {
        color: '#323232',
        fontFamily: 'Roboto-Regular'
    },
    panhandlercontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        elevation: 2,
    },
    summarydatacontainer: {
      height: '100%',
      width: '100%',
      position: 'absolute',
      left: '0%',
      top: '0%',  
    },
    summaryheadercontainer: {
        height: '100%',
        width: '45%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        backgroundColor: '#323232',
        elevation: 2,
        borderRadius: GetScreenHeight(1)
    },
    summaryheadersubcontainer: {
        height: '15%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    summaryheader: {
        color: 'white',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: DynamicFont(55),
        textAlign: 'center',
    },
    summaryamountcontainer: {
        height: '100%',
        width: '42.5%',
        position: 'absolute',
        right: '5%',
        top: '0%',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
        backgroundColor: 'white',
    },
    summaryamountheadercontainer: {
        height: '15%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    summaryamountheader: {
        color: '#323232',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: DynamicFont(55),
        textAlign: 'center',
    },
    payslipfooter: {
        height: '12.5%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        bottom: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    extrainformationcontainer: {
        height: '100%', 
        width: '35%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    extrainformationtopheader: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    extrainformationbottomheader: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '50%',
        alignItems: 'center',
    },
    extrainformationheader: {
        color: '#323232',
        fontFamily: 'Montserrat-Bold',
        fontSize: DynamicFont(50),
    },
    dotscontainer: {
        height: '100%',
        width: '35%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    dot: {
        height: GetScreenHeight(0.75),
        width: GetScreenHeight(0.75),
        backgroundColor: '#323232',
        borderRadius: 500,
        marginRight: GetScreenWidth(0.5),
        marginLeft: GetScreenWidth(0.5)
    },
    backbuttoncontainer: {
        height: '100%',
        width: '30%',
        position: 'absolute',
        right: '5%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backbutton: {
        height: '65%',
        width: '65%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: GetScreenHeight(1),
        borderWidth: 1, 
        borderColor: '#323232',
    },
    backbuttontext: {
        color: '#323232',
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(40),
    }
})