import React, {Component} from "react";
import {View, StyleSheet, Image, Text, TouchableWithoutFeedback, Animated, Easing} from "react-native";
import axios from "axios";
import {DynamicFont, GetScreenWidth, GetScreenHeight} from "./ResponsiveFont";

export default class PayslipInput extends Component {
    constructor(){
        super();
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateMonth: new Animated.Value(0),
            animateFilter: new Animated.Value(0),
            animateMonthText: new Animated.Value(0),
            animateMonthLeftArrow: new Animated.Value(0),
            animateMonthCenterText: new Animated.Value(0),
            animateMonthRightArrow: new Animated.Value(0),
            animateYearText: new Animated.Value(0),
            animateYearLeftArrow: new Animated.Value(0),
            animateYearCenterText: new Animated.Value(0),
            animateYearRightArrow: new Animated.Value(0),
            animateFilterButton: new Animated.Value(0),
            animateFilterContent: new Animated.Value(0),
            animateErrorMessage: new Animated.Value(0),

            //standard variables
            month: 1,
            monthName: 'Jan',
            year: 2020,
            errorMessage: 'Please make sure an Employee is selected.',

            //load variables
        }   
    }

    componentDidMount(){

    }

    DecreaseMonth(){
        let month = this.state.month;
        if(month === 1){
            month = 12;
        }
        else{
            month--;
        }
        this.setState({
            month,
            monthName: this.GetMonth(month),
        })
    }

    IncreaseMonth(){
        let month = this.state.month;
        if(month === 12){
            month = 1;
        }
        else{
            month++;
        }
        this.setState({
            month,
            monthName: this.GetMonth(month),
        })
    }

    DecreaseYear(){
        let year = this.state.year;
        year--;
        this.setState({
            year,
        })
    }

    IncreaseYear(){
        let year = this.state.year;
        year++;
        this.setState({
            year
        })
    } 

    GetMonth(month){
        if(month === 1) {return 'Jan'}
        else if(month === 2) {return 'Feb'}
        else if(month === 3) {return 'Mar'}
        else if(month === 4) {return 'Apr'}
        else if(month === 5) {return 'May'}
        else if(month === 6) {return 'Jun'}
        else if(month === 7) {return 'Jul'}
        else if(month === 8) {return 'Aug'}
        else if(month === 9) {return 'Sep'}
        else if(month === 10) {return 'Oct'}
        else if(month === 11) {return 'Nov'}
        else if(month === 12) {return 'Dec'}
    }

    FilterButton(){
        this.AnimateFilterButton();
        this.GetPayslip();
        this.HideErrorMessage();
    }

    //API calls here
    GetPayslip(){
        let data = [];
        axios.post('http://dev-buetk-backend.softperts.com/api/Payslip/list',{
            month: {
                year: this.state.year,
                month: this.state.month,
                day: 1,
            },
            branchId: 24,
            employeeId: 27,
        }).then(response => {
            data = response.data;
            if(data.salaryStatus === null){
                this.setState({
                    errorMessage: 'No salary record found.'
                })
                setTimeout(() => {
                    Animated.timing(this.state.animateFilter).stop();
                    this.ResetFilterButtonAnimation();
                    this.ShowErrorMessage();
                }, 3000);
            }
            else{
                //navigation
                setTimeout(() => {
                    Animated.timing(this.state.animateFilter).stop();
                    this.ResetFilterButtonAnimation();
                    this.AnimateHide(true, data, this.state.month, this.state.year);
                }, 3000);
            }
        }).catch(error => {
            console.log(error)
        })
    }

    //animated functions
    AnimateShow(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateMonth, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFilter, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateHide(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateFilter, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateMonth, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateHide(state, data, month, year){
        Animated.stagger(100, [
            Animated.timing(this.state.animateFilter, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateMonth, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start(() => {
            this.props.StoreData(state, data, month, year);
        });
    }

    AnimateMonthToDateInput(){
        Animated.stagger(400, [
            Animated.timing(this.state.animateMonthText, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateMonthLeftArrow, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthRightArrow, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateMonthCenterText, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ]).start();
    }

    AnimateYearToDateInput(){
        Animated.stagger(400, [
            Animated.timing(this.state.animateYearText, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.stagger(100, [
                Animated.timing(this.state.animateYearLeftArrow, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearRightArrow, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateYearCenterText, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ]).start();
    }   

    AnimateFilterButton(){
        this.HideErrorMessage();
        Animated.stagger(100, [
            Animated.timing(this.state.animateFilterContent, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
            }),
            Animated.timing(this.state.animateFilter, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFilterButton, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            })
        ]).start(()=> {
            this.AnimateFilterButtonLoop();
        });
    }

    ResetFilterButtonAnimation(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateFilter, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFilterButton, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateFilterContent, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
            })
        ]).start();
    }

    AnimateFilterButtonLoop(){
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animateFilter, {
                    toValue: 300,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateFilter, {
                    toValue: 200,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true
                })
            ])
        ).start();
    }

    ShowErrorMessage(){
        Animated.timing(this.state.animateErrorMessage, {
            toValue: 100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }

    HideErrorMessage(){
        Animated.timing(this.state.animateErrorMessage, {
            toValue: 0,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }
    
    render(){
        return(
            <View style = {styles.container}>
                <Animated.View style = {[styles.monthcontainer, {opacity: this.state.animateMonth.interpolate({
                    inputRange: [0, 100],
                    outputRange: [0, 1]
                }), transform: [{translateX: this.state.animateMonth.interpolate({
                    inputRange: [0, 100],
                    outputRange: [-15, 0]
                })}]}]}>
                    <Text style = {styles.month}>Month</Text>
                    <View style = {styles.monthinputcontainer}>
                        <TouchableWithoutFeedback onPress = {this.AnimateMonthToDateInput.bind(this)}>
                            <View style = {[styles.monthsubinputcontainer, styles.monthsubinputcontainerone]}>
                                <Animated.View style = {[styles.monthinput, {transform: [{scaleX: this.state.animateMonthText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0.5, 0.75]
                                })}, {scaleY: this.state.animateMonthText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0.5, 0.8]
                                })}]}]}>
                                    <Animated.View style = {[styles.monthinputbackground, {opacity: this.state.animateMonthText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [0, 1]
                                    })}]} />
                                </Animated.View>
                                
                                <Animated.View style = {[styles.monthtextcontainer, {opacity: this.state.animateMonthText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [1, 0]
                                }), transform: [{translateY: this.state.animateMonthText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0, 10]
                                })}]}]}>
                                    <Text style = {styles.monthtext}>Month</Text>
                                </Animated.View>
                                <View style = {styles.dateinputcontainer}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseMonth.bind(this)}>
                                        <Animated.View style = {[styles.dateinputleftarrowcontainer, {opacity: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [0, 1]
                                        }), transform: [{translateY: this.state.animateMonthLeftArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [-10, 0]
                                        })}]}]}>
                                            <Image style = {styles.arrow} resizeMode = "contain" source = {require('./resources/arrowleft.png')} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputcentercontainer, {opacity: this.state.animateMonthCenterText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [0, 1]
                                    }), transform: [{translateY: this.state.animateMonthCenterText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [-10, 0]
                                    })}]}]}>
                                        <Text style = {styles.dateinputtext}>{this.state.monthName}</Text>
                                    </Animated.View>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseMonth.bind(this)}>
                                        <Animated.View style = {[styles.dateinputrightarrowcontainer, {opacity: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [0, 100,],
                                            outputRange: [0, 1]
                                        }), transform: [{translateY: this.state.animateMonthRightArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [-10, 0]
                                        })}]}]}>
                                            <Image style = {[styles.arrow, {transform: [{rotate: '180deg'}]}]} resizeMode = "contain" source = {require('./resources/arrowleft.png')} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress = {this.AnimateYearToDateInput.bind(this)}>
                            <View style = {[styles.monthsubinputcontainer, styles.monthsubinputcontainertwo]}>
                                <Animated.View style = {[styles.monthinput, {transform: [{scaleX: this.state.animateYearText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0.5, 0.75]
                                })}, {scaleY: this.state.animateYearText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0.5, 0.8]
                                })}]}]}>
                                    <Animated.View style = {[styles.monthinputbackground, {opacity: this.state.animateYearText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [0, 1]
                                    })}]} />
                                </Animated.View>
                                
                                <Animated.View style = {[styles.monthtextcontainer, {opacity: this.state.animateYearText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [1, 0]
                                }), transform: [{translateY: this.state.animateYearText.interpolate({
                                    inputRange: [0, 100],
                                    outputRange: [0, 10]
                                })}]}]}>
                                    <Text style = {styles.monthtext}>Year</Text>
                                </Animated.View>
                                <View style = {styles.dateinputcontainer}>
                                    <TouchableWithoutFeedback onPress = {this.DecreaseYear.bind(this)}>
                                        <Animated.View style = {[styles.dateinputleftarrowcontainer, {opacity: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [0, 1]
                                        }), transform: [{translateY: this.state.animateYearLeftArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [-10, 0]
                                        })}]}]}>
                                            <Image style = {styles.arrow} resizeMode = "contain" source = {require('./resources/arrowleft.png')} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    <Animated.View style = {[styles.dateinputcentercontainer, {opacity: this.state.animateYearCenterText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [0, 1]
                                    }), transform: [{translateY: this.state.animateYearCenterText.interpolate({
                                        inputRange: [0, 100],
                                        outputRange: [-10, 0]
                                    })}]}]}>
                                        <Text style = {styles.dateinputtext}>{this.state.year}</Text>
                                    </Animated.View>
                                    <TouchableWithoutFeedback onPress = {this.IncreaseYear.bind(this)}>
                                        <Animated.View style = {[styles.dateinputrightarrowcontainer, {opacity: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [0, 100,],
                                            outputRange: [0, 1]
                                        }), transform: [{translateY: this.state.animateYearRightArrow.interpolate({
                                            inputRange: [0, 100],
                                            outputRange: [-10, 0]
                                        })}]}]}>
                                            <Image style = {[styles.arrow, {transform: [{rotate: '180deg'}]}]} resizeMode = "contain" source = {require('./resources/arrowleft.png')} />
                                        </Animated.View>
                                    </TouchableWithoutFeedback>
                                    </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </Animated.View>
                <Animated.View style = {[styles.filterbuttoncontainer, {opacity: this.state.animateFilter.interpolate({
                    inputRange: [0 ,100, 200, 300],
                    outputRange: [0, 1, 0.65, 1]
                }), transform: [{translateX: this.state.animateFilter.interpolate({
                    inputRange: [0, 100, 200, 300],
                    outputRange: [10, 0, -GetScreenWidth(35), -GetScreenWidth(35)]
                })}, {rotate: this.state.animateFilter.interpolate({
                    inputRange: [0, 100, 200, 300],
                    outputRange: [0, 0, '-180deg', '-180deg']
                })}, {scale: this.state.animateFilter.interpolate({
                    inputRange: [0, 100, 200 ,300],
                    outputRange: [1, 1, 0.5, 1]
                })}]}]}>
                    <TouchableWithoutFeedback onPress = {this.FilterButton.bind(this)}>
                        <Animated.View style = {[styles.filterbutton, {borderRadius: this.state.animateFilterButton.interpolate({
                            inputRange: [0, 100],
                            outputRange: [GetScreenHeight(1), 500]
                        }), transform: [{scale: this.state.animateFilterButton.interpolate({
                            inputRange: [0, 100],
                            outputRange: [1, 0.5]
                        })}]}]}>
                            <Animated.Text style = {[styles.filterbuttontext, {opacity: this.state.animateFilterContent.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            })}]}>Filter</Animated.Text>
                            <Animated.Image style = {[styles.filterarrow, {opacity: this.state.animateFilterContent.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            })}]} source = {require('./resources/arrowfilterright.png')} />
                        </Animated.View>
                    </TouchableWithoutFeedback>
                </Animated.View>
                <Animated.View style = {[styles.errormessagecontainer, {opacity: this.state.animateErrorMessage.interpolate({
                    inputRange: [0, 100],
                    outputRange: [0, 1]
                }), transform: [{translateY: this.state.animateErrorMessage.interpolate({
                    inputRange: [0, 100],
                    outputRange: [10, 0]
                })}]}]}>
                    <Text style = {styles.errormessage}>{this.state.errorMessage}</Text>
                </Animated.View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '30%',
    },
    monthcontainer: {
        height: '25%',
        width: '45%',
        position: 'absolute',
        left: '5%',
        top: '0%',
        backgroundColor: 'white',
        elevation: 2,
        borderRadius: GetScreenHeight(1.5),
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    month: {
        color: '#323232',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(65),
        position: 'absolute',
        top: '5%',
        left: '5%',
    },
    monthinputcontainer: {
        height: '75%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '25%',
    },
    monthsubinputcontainer: {
        height: '50%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    monthsubinputcontainerone: {
        top: '0%',
    },
    monthsubinputcontainertwo: {
        top: '50%',
    },
    monthinput: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent :'center',
        alignItems: 'center',
        borderRadius: GetScreenHeight(1),
        borderWidth: 2,
        borderColor: '#323232'
    },
    monthinputbackground: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: '#323232',
    },
    monthtextcontainer: {
        height: '50%',
        width: '50%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    monthtext: {
        color: '#323232',
        fontFamily: 'Montserrat-Medium',
        fontSize: DynamicFont(35),
    },
    dateinputcontainer: {
        height: '80%',
        width: '75%',
        position: 'absolute',
        justifyContent :'center',
        alignItems: 'center',
        borderRadius: GetScreenHeight(1),
        overflow: 'hidden',
    },
    dateinputleftarrowcontainer: {
        height: '100%',
        width: '25%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateinputrightarrowcontainer: {
        height: '100%',
        width: '25%',
        position: 'absolute',
        right: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateinputcentercontainer: {
        height: '100%',
        width: '50%',
        position: 'absolute',
        left: '25%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    arrow: {
        height: '75%',
        width: '75%',
        position: 'absolute',
        tintColor: 'white',
    },
    dateinputtext: {
        color: 'white',
        fontFamily: 'Montserrat-Bold',
        fontSize: DynamicFont(40)
    },
    filterbuttoncontainer: {
        height: GetScreenHeight(12.5),
        width: GetScreenHeight(12.5),
        position: 'absolute',
        right: '5%', 
        bottom: '35%',
    },
    filterbutton: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
    },
    filterbuttontext: {
        color: 'white',
        fontSize: DynamicFont(45),
        fontFamily: 'OpenSans-Regular',
        position: 'absolute',
        left: '10%',
        top: '10%',
    },
    filterarrow: {
        tintColor: 'white',
        position: 'absolute',
        bottom: '0%',
        right: '5%',
        transform: [{scaleY: 0.5}]
    },
    errormessagecontainer: {
        height: '5%',
        width: '50%',
        position: 'absolute',
        left: '5%',
        bottom: '37.5%',
        backgroundColor: '#323232',
        borderRadius: 500,
        justifyContent: 'center',
        alignItems: 'center',
    },
    errormessage: {
        width: '90%',
        color: 'white',
        fontFamily: 'Montserrat-Regular',
        fontSize: DynamicFont(30),
        textAlign: 'center',
    },
})