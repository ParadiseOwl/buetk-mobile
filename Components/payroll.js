import React, {Component} from "react";
import {View, StyleSheet, Image, ImageBackground, Easing, Animated, Text, TextInput, TouchableWithoutFeedback, ScrollView, PanResponder} from "react-native";
import {DynamicFont, GetScreenHeight, GetScreenWidth} from "./ResponsiveFont";
import axios from "axios";

//pages import
import Payslip from './payslip';
import PayslipInput from './payslipinput';
import LeaveRequest from "./LeaveRequest/leaverequest";
import AddLeaveRequest from './AddLeaveRequest/addleaverequest';

export default class Payroll extends Component {
    selectedPage = 0;
    constructor(){
        super()
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateHeader: new Animated.Value(0),
            animateSideMenu: new Animated.Value(0),
            animateMenu: new Animated.Value(0),

            //standard variables
            payslipData: [],

            //load variables
            isLoaded: false,
            enablePayslipInput: true,
            enablePayslip: false,
            enableAddLeaveRequest: false,
            enableLeaveRequest: false,
        }
        this.AnimateShowPartial = this.AnimateShowPartial.bind(this);

        let pos = 0
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderMove: (evt, gestureState) => {
                if(pos + gestureState.dx < 0 && pos + gestureState.dx > -GetScreenWidth(115) ){
                    this.state.animateSideMenu.setValue(pos + gestureState.dx);
                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                if(pos + gestureState.dx < 0 && pos + gestureState.dx > -GetScreenWidth(115)){
                    pos = pos + gestureState.dx;
                }
                else if(pos + gestureState.dx < -GetScreenWidth(115)){
                    pos = -GetScreenWidth(114.50);
                    Animated.timing(this.state.animateSideMenu, {
                        toValue: pos,
                        duration: 200,
                        easing: this.state.easing,
                        useNativeDriver: true
                    }).start();
                }
                else{
                    pos = 0;
                    Animated.timing(this.state.animateSideMenu, {
                        toValue: 0,
                        duration: 200,
                        easing: this.state.easing,
                        useNativeDriver: true,
                    }).start();
                }
            }
        })
    }

    //Standard functions
    //here

    componentDidMount(){
        this.ChangePage(this.refs.payslipinput);
    }   

    ChangePage(page){
        page.AnimateHide();
    }

    AnimateHidePage(obj){
        obj.AnimateHide();
    }

    AnimateShowPage(page){

    }

    //Animated Functions
    //Here

    AnimateShow(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateMenu, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start(
            ()=> {
                this.refs.payslipinput.AnimateShow();
            }
        );
    }

    AnimateHide(){
        this.refs.payslipinput.AnimateHide();
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateMenu, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
        ]).start();
    }

    AnimateShowPartial(){
        this.setState({
            enablePayslip: false,
            enablePayslipInput: true,
        });
        this.refs.payslipinput.AnimateShow();
    }
    
    AnimateHidePartial(state, data, month, year){
        let date = {
            currentMonth: month,
            currentYear: year
        };
        let newData = {
            ...data,
            ...date
        }
        this.setState({
            enablePayslipInput: false,
            enablePayslip: state,
            payslipData: newData,
        })
    }

    ChangePage(){
        this.props.ChangePageToHome();
        this.AnimateHide();
    }

    
    render(){
        let selectedbuttonbackground = {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'};
        let selectedbuttontext = {color: '#323232'}
        return(
            <View style = {styles.container}>
                <View style = {styles.viewport}>
                    <Animated.View style = {[styles.payrollcontainer, {opacity: this.state.animateHeader.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateX: this.state.animateHeader.interpolate({
                        inputRange: [0, 100],
                        outputRange: [-20, 0]
                    })}]}]}>
                        <TouchableWithoutFeedback onPress = {this.ChangePage.bind(this)}>
                            <Text style = {styles.payroll}>Payroll</Text>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                    <Animated.View style = {[styles.sidewaysmenucontainer, {opacity: this.state.animateMenu.interpolate({
                        inputRange: [0, 100],
                        outputRange: [0, 1]
                    }), transform: [{translateX: this.state.animateMenu.interpolate({
                        inputRange: [0, 100],
                        outputRange: [GetScreenWidth(20), 0]
                    })}]}]}>
                        <View style = {styles.sidewaysmenuscrollcontainerpanhandler} {...this.panResponder.panHandlers}>
                            <Animated.View style = {[styles.sidewaysmenuscrollcontainer, {transform: [{translateX: this.state.animateSideMenu}]}]} >
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <TouchableWithoutFeedback>
                                        <View style = {[styles.sidewaysmenubutton, this.state.enablePayslipInput ? null : selectedbuttonbackground]}>
                                            <Text style = {[styles.sidewaysmenubuttontext, this.state.enablePayslipInput ? null : selectedbuttontext]}>Payslip</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <View style = {[styles.sidewaysmenubutton, {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'}]}>
                                        <Text style = {[styles.sidewaysmenubuttontext, {color: '#323232'}]}>Leave Request</Text>
                                    </View>
                                </View>
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <View style = {[styles.sidewaysmenubutton, {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'}]}>
                                        <Text style = {[styles.sidewaysmenubuttontext, {color: '#323232'}]}>Trip Request</Text>
                                    </View>
                                </View>
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <View style = {[styles.sidewaysmenubutton, {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'}]}>
                                        <Text style = {[styles.sidewaysmenubuttontext, {color: '#323232'}]}>House Maintenance Request</Text>
                                    </View>
                                </View>
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <View style = {[styles.sidewaysmenubutton, {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'}]}>
                                        <Text style = {[styles.sidewaysmenubuttontext, {color: '#323232'}]}>Loan Request</Text>
                                    </View>
                                </View>
                                <View style = {[styles.sidewaysmenubuttoncontainer]}>
                                    <View style = {[styles.sidewaysmenubutton, {backgroundColor: 'transparent', borderWidth: 1, borderColor: '#323232'}]}>
                                        <Text style = {[styles.sidewaysmenubuttontext, {color: '#323232'}]}>Personal Information Review</Text>
                                    </View>
                                </View>
                            </Animated.View>
                        </View>
                    </Animated.View>
                    {this.state.enablePayslipInput && (
                        <PayslipInput ref = "payslipinput" userData = {this.props.userData} StoreData = {this.AnimateHidePartial.bind(this)} />
                    )}
                    {this.state.enablePayslip && (
                        <Payslip ref = "payslip" AnimateShowPartial = {this.AnimateShowPartial} payslipData = {this.state.payslipData} />
                    )}
                    {this.state.enableAddLeaveRequest && (
                        <AddLeaveRequest ref = "addleaverequest" loginData = {this.props.loginData} />
                    )}
                    {this.state.enableLeaveRequest && (
                        <LeaveRequest ref = "leaverequest" />
                    )}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute', 
        left: '0%',
        top: '0%',
    },
    payrollcontainer: {
        position: 'absolute',
        left: '5%',
        top: '10%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    payroll: {
        color: '#323232',
        fontFamily: 'Nunito-Black',
        fontSize: DynamicFont(100)
    },
    sidewaysmenucontainer: {
        height: '5%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '20%',
    },
    sidewaysmenuscrollcontainerpanhandler:{
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: 'rgba(250, 250, 250, 0)',
    },
    sidewaysmenuscrollcontainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        flexDirection: 'row'
    },
    sidewaysmenubuttoncontainer: {
        height: '100%',
        marginLeft: GetScreenWidth(3.5),
    },
    sidewaysmenubutton: {
        height: '100%',
        borderRadius: 500,
        backgroundColor: '#323232',
    },
    sidewaysmenubuttontext: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        margin: GetScreenHeight(1),
        marginLeft: GetScreenHeight(2),
        marginRight: GetScreenHeight(2),
        fontSize: DynamicFont(35),
    },
    //to calculate the panel width you add the total margin left and right, the width, and marginLeft on the parent container
    //the font to width ratio is 1.6667
    panHandlerContainer:{
        height: '10%',
        width: '15%',
        position: 'absolute',
        left: '42.5%',
        top: '2%',
        backgroundColor: '#323232',
        opacity: 0,
    },
})