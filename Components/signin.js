import React, {Component, useRef} from "react";
import {View, StyleSheet, Text, ImageBackground, Image, Animated, Easing, TextInput, TouchableWithoutFeedback, Dimensions, KeyboardAvoidingView, Alert} from "react-native";
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

export default class SignIn extends Component{
    constructor(){
        super();
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            animateUsernameField: new Animated.Value(0),
            animatePasswordField: new Animated.Value(0),
            animateUsernamePlaceholder: new Animated.Value(0),
            animatePasswordPlaceholder: new Animated.Value(0),
            contentTransition: new Animated.Value(0),
            animateBubbles: new Animated.Value(0),
            animatePage: new Animated.Value(0),
            animateDot: new Animated.Value(0),
            animateDotXPos: new Animated.Value(0),
            animateDotYPos: new Animated.Value(0),
            animateUsernameErrorLine: new Animated.Value(0),
            animatePasswordErrorLine: new Animated.Value(0),
            usernameText: "",
            passwordText: "",
            data: [],
        }
    }

    componentDidMount(){
        this.AnimateBubbles();
    }

    Login(){
        let data = [];
        let areFieldsFilled = true;

        if(this.state.usernameText === ""){
            areFieldsFilled = false;
            this.AnimateUsernameErrorLine();
        }
        if(this.state.passwordText === ""){
            areFieldsFilled = false;
            this.AnimatePasswordErrorLine();
        }
        if(areFieldsFilled){
            this.AnimatePageToHidden();
            this.HideErrorLines();
            axios.post('http://dev-buetk-backend.softperts.com/api/User/Login',{
                UserName: this.state.usernameText,
                Password: this.state.passwordText,
            }).then((response) => {
                data = response.data;
                data = JSON.stringify(data);
                AsyncStorage.setItem('user-data', data);
                let that = this;
                setTimeout(() => {
                    that.props.navigation.navigate('Dashboard');
                }, 3000);
            }).catch(error => {
                console.log(error);
                setTimeout(() => {
                    this.ResetDotPos();
                }, 3000);
            })
        }
    }

    AnimatePageToHidden(){
        Animated.timing(this.state.animatePage, {
            toValue: 100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start(()=> {
            this.AnimateDotPos();
        });
    }

    AnimatePageToShow(){
        Animated.timing(this.state.animateDot).stop();
        Animated.timing(this.state.animatePage, {
            toValue: 0,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true
        }).start();
    }

    AnimateDot(){
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animateDot, {
                    toValue: 100,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateDot, {
                    toValue: 0,
                    duration: 600,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ])
        ).start();
    }

    AnimateDotPos(){
        Animated.parallel([
            Animated.timing(this.state.animateDotXPos, {
                toValue: 100,
                duration: 600,
                easing: Easing.bezier(0.165, 0.84, 0.44, 1),
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateDotYPos, {
                toValue: 100,
                duration: 600,
                easing: Easing.bezier(0.25, 0.46, 0.45, 0.94),
                useNativeDriver: true,
            })
        ]).start(()=> {
            this.AnimateDot();
        });
    }

    ResetDotPos(){
        Animated.parallel([
            Animated.timing(this.state.animateDotXPos, {
                toValue: 0,
                duration: 600,
                easing: Easing.bezier(0.165, 0.84, 0.44, 1),
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateDotYPos, {
                toValue: 0,
                duration: 600,
                easing: Easing.bezier(0.25, 0.46, 0.45, 0.94),
                useNativeDriver: true,
            })
        ]).start(()=> {
            this.AnimatePageToShow();
        });
    }

    AnimateUsernameField(){
        Animated.sequence([
            Animated.parallel([
                Animated.timing(this.state.contentTransition, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateUsernamePlaceholder, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animateUsernameField, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimatePasswordField(){
        Animated.sequence([
            Animated.parallel([
                Animated.timing(this.state.contentTransition, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animatePasswordPlaceholder, {
                    toValue: 100,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                })
            ]),
            Animated.timing(this.state.animatePasswordField, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    ResetLayout(){
        this.HideErrorLines();
        Animated.sequence([
            Animated.timing(this.state.contentTransition, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.parallel([
                Animated.timing(this.state.animateUsernameField, {
                    toValue: 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animatePasswordField, {
                    toValue: 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animateUsernamePlaceholder, {
                    toValue: this.state.usernameText !== "" ? 100 : 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }),
                Animated.timing(this.state.animatePasswordPlaceholder, {
                    toValue: this.state.passwordText !== "" ? 100 : 0,
                    duration: 800,
                    easing: this.state.easing,
                    useNativeDriver: true,
                }) 
            ])
        ]).start();
    }

    AnimateBubbles(){
        Animated.stagger(600, [
            Animated.timing(this.state.animateBubbles, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateBubbles, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateUsernameErrorLine(){
        Animated.timing(this.state.animateUsernameErrorLine, {
            toValue: 100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }

    AnimatePasswordErrorLine(){
        Animated.timing(this.state.animatePasswordErrorLine, {
            toValue :100,
            duration: 800,
            easing: this.state.easing,
            useNativeDriver: true,
        }).start();
    }

    HideErrorLines(){
        Animated.parallel([
            Animated.timing(this.state.animateUsernameErrorLine, {
                toValue: 0,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animatePasswordErrorLine, {
                toValue: 0,
                duration: 800,
                eaisng: this.state.easing,
                useNativeDriver: true
            })
        ]).start();
    }

    render(){
        return(
            <KeyboardAvoidingView style = {styles.container} keyboardVerticalOffset = {500} >
                <View style = {styles.container}>
                    <Animated.View style = {[styles.viewport, {opacity: this.state.animatePage.interpolate({
                        inputRange: [0, 100],
                        outputRange: [1, 0]
                    }), transform: [{scaleX: this.state.animatePage.interpolate({
                        inputRange: [0, 100],
                        outputRange: [1, 0.9],
                    })}, {scaleY: this.state.animatePage.interpolate({
                        inputRange: [0, 100],
                        outputRange: [1, 0.9]
                    })}]}]}>
                        <Animated.View style = {[styles.bubbletwo, {opacity: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 0, 1]
                        }), transform: [{translateX: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-Dimensions.get('screen').width * 0.5, -Dimensions.get('screen').width * 0.5, 0]
                        })},{translateY: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-Dimensions.get('screen').height * 0.3, -Dimensions.get('screen').height * 0.3, 0]
                        })}]}]} >
                            <ImageBackground source = {require('./resources/bubbletwo.jpg')} style = {styles.bubbleimage} />
                        </Animated.View>
                        <Animated.View style = {[styles.bubbleone, {opacity: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 1]
                        }), transform: [{translateX: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [Dimensions.get('screen').width * 0.5, 0, 0]
                        })}, {translateY: this.state.animateBubbles.interpolate({
                            inputRange: [0, 100, 200,],
                            outputRange: [-Dimensions.get('screen').height * 0.3, 0, 0]
                        })}]}]}>
                            <ImageBackground source = {require('./resources/bubbletwo.jpg')} style = {styles.bubbleimage} />
                        </Animated.View>
                        <Animated.View style = {[styles.headercontainer, {opacity: this.state.contentTransition.interpolate({
                            inputRange: [0, 100],
                            outputRange: [1, 0]
                        }), transform: [{translateX: this.state.contentTransition.interpolate({
                            inputRange: [0, 100],
                            outputRange: [0, -10]
                        })}]}]}>
                            <Text style = {[styles.header, styles.headerone]}>Welcome,</Text>
                            <Text style = {[styles.header, styles.headertwo]}>sign in to continue</Text>
                        </Animated.View>

                        <Animated.View style = {[styles.fieldcontainer, styles.usernamecontainer, {transform: [{translateY: this.state.contentTransition.interpolate({
                            inputRange: [0, 100],
                            outputRange: [0, -Dimensions.get('screen').height * 0.2]
                        })}]}]}>
                            <Animated.Text style = {[styles.fieldplaceholdertext, {opacity: this.state.animateUsernamePlaceholder.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            }), transform: [{translateY: this.state.animateUsernamePlaceholder.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, -10]
                            })}]}]}>Username</Animated.Text>
                            <View style = {styles.fieldbottomline} />
                            <Animated.View style = {[styles.fieldbottomerrorline, {transform: [{translateX: this.state.animateUsernameErrorLine.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, Dimensions.get('screen').width * 0.9]
                            })}]}]} />
                            <Animated.View style = {[styles.fieldbottomline, {backgroundColor: '#323232', height: 2, left: '-100%', transform: [{translateX: this.state.animateUsernameField.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, Dimensions.get('screen').width * 0.9]
                            })}]}]} />
                            <TextInput onChangeText = {(text) => this.setState({usernameText: text})} style = {styles.field} onBlur = {this.ResetLayout.bind(this)} onFocus = {this.AnimateUsernameField.bind(this)} />
                        </Animated.View>

                        <Animated.View style = {[styles.fieldcontainer, styles.passwordcontainer, {transform: [{translateY: this.state.contentTransition.interpolate({
                            inputRange: [0, 100],
                            outputRange: [0, -Dimensions.get('screen').height * 0.2]
                        })}]}]}>
                            <Animated.Text style = {[styles.fieldplaceholdertext, {opacity: this.state.animatePasswordPlaceholder.interpolate({
                                inputRange: [0, 100],
                                outputRange: [1, 0]
                            }), transform: [{translateY: this.state.animatePasswordPlaceholder.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, -10]
                            })}]}]}>Password</Animated.Text>
                            <View style = {styles.fieldbottomline} />
                            <Animated.View style = {[styles.fieldbottomerrorline, {transform: [{translateX: this.state.animatePasswordErrorLine.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, Dimensions.get('screen').width * 0.9]
                            })}]}]} />
                            <Animated.View style = {[styles.fieldbottomline, {backgroundColor: '#323232', height: 2, left: '-100%', transform: [{translateX: this.state.animatePasswordField.interpolate({
                                inputRange: [0, 100],
                                outputRange: [0, Dimensions.get('screen').width * 0.9]
                            })}]}]} />
                            <TextInput secureTextEntry = {true} onChangeText = {(text) => {this.setState({passwordText: text})}} style = {styles.field} onBlur = {this.ResetLayout.bind(this)} onFocus = {this.AnimatePasswordField.bind(this)} />
                        </Animated.View>
                        <TouchableWithoutFeedback onPress = {this.Login.bind(this)}>
                            <View style = {styles.signinbox}>
                                <View style = {styles.signinbutton} />
                                <Text style = {styles.signintext}>Sign In</Text>
                                <Image style = {styles.arrow} resizeMode = "contain" source = {require('./resources/arrowright.png')} />
                            </View>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                    <Animated.View style = {[styles.dot, {opacity: this.state.animateDot.interpolate({
                        inputRange: [0, 100],
                        outputRange: [1, 0.75]
                    }), transform: [{scaleX: this.state.animateDot.interpolate({
                        inputRange: [0, 100],
                        outputRange: [1, 0.5]
                    })}, {scaleY: this.state.animateDot.interpolate({
                        inputRange: [0 ,100],
                        outputRange: [1, 0.5]
                    })}, {translateX: this.state.animateDotXPos.interpolate({
                        inputRange: [0, 100],
                        outputRange: [Dimensions.get('screen').width * 0.6, 0]
                    })}, {translateY: this.state.animateDotYPos.interpolate({
                        inputRange: [0, 100],
                        outputRange: [Dimensions.get('screen').height * 0.2, 0],
                    })}]}]} />
                </View>
            </KeyboardAvoidingView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('screen').height,
        width: Dimensions.get('screen').width,
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    bubbleone: {
        height: '50%',
        width: Dimensions.get('screen').height * 0.5,
        position: 'absolute',
        right: -Dimensions.get('screen').width * 0.25,
        top: '-25%',
        borderRadius: 500,
        backgroundColor: '#42A5F5',
        overflow: 'hidden'
    },
    bubbletwo: {
        height: '30%',
        width: Dimensions.get('screen').height * 0.3,
        position: 'absolute',
        left: Dimensions.get('screen').width * 0,
        top: '-15%',
        borderRadius: 500,
        backgroundColor: '#42f593',
        overflow: 'hidden',
        transform: [
            {rotate: '90deg'}
        ]
    },
    bubbleimage: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    headercontainer: {
        position: 'absolute',
        left: '10%',
        top: '22.5%',
        flexDirection: 'column',
    },
    header: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 28,
    },
    headerone: {
        color: '#323232'
    },
    headertwo: {
        color: '#969696',
    },
    fieldcontainer: {
        height: '7%',
        width: '90%',
        position: 'absolute',
        left: '10%',
        overflow: 'hidden'
    },
    usernamecontainer: {
        top: '45%',
    },
    passwordcontainer: {
        top: '57.5%',
    },
    fieldplaceholdertext: {
        color: '#969696',
        fontSize: 14, 
        fontFamily: 'ProximaNova-Regular'
    },  
    fieldbottomline: {
        height: 1,
        width: '100%',
        backgroundColor: '#969696',
        position: 'absolute',
        left: '0%',
        bottom: '0%',
    },
    fieldbottomerrorline: {
        height: 1,
        width: '100%',
        backgroundColor: 'red',
        position: 'absolute',
        left: '-100%',
        bottom: '0%',
    },
    field: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    signinbox: {
        height: '9%',
        width: '60%',
        position: 'absolute',
        left: '10%',
        bottom: '20%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    signinbutton: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',backgroundColor: '#323232',
        opacity: 0.1,
        borderRadius: 10,
    },
    signintext: {
        color: '#646464',
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        marginLeft: '20%'
    },
    arrow: {
        height: '35%',
        width: '30%',
        top: '1%',
        tintColor: '#646464',
        transform: [{scaleY: 0.75}, {scaleX: 1.25}]
    },
    dot: {
        height: '5%',
        width: Dimensions.get('screen').height * 0.05,
        position: 'absolute',
        backgroundColor: '#323232',
        borderRadius: 500,
    }
})