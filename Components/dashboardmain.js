import React, {Component} from "react";
import {View, StyleSheet, Image, ImageBackground, Animated, Easing, Text, TextInput, TouchableWithoutFeedback} from "react-native";;
import {DynamicFont, GetScreenHeight, GetScreenWidth} from "./ResponsiveFont";

export default class DashboardMain extends Component {
    constructor(){
        super()
        this.state = {
            easing: Easing.bezier(0.77, 0, 0.175, 1),
            //animated variables
            animateHeader: new Animated.Value(0),
            animatePayroll: new Animated.Value(0),
            animateBoxOne: new Animated.Value(0),
            animateBoxTwo: new Animated.Value(0),
            animateBoxThree: new Animated.Value(0),
            animateBoxfour: new Animated.Value(0),
            animateSidePanel: new Animated.Value(0),
        }
    }

    AnimateShow(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animatePayroll, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBoxTwo, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateBoxOne, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateBoxfour, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBoxThree, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateSidePanel, {
                toValue: 100,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    AnimateHide(){
        Animated.stagger(100, [
            Animated.timing(this.state.animateHeader, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animatePayroll, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBoxOne, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateBoxTwo, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateBoxThree, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true
            }),
            Animated.timing(this.state.animateBoxfour, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            }),
            Animated.timing(this.state.animateSidePanel, {
                toValue: 200,
                duration: 800,
                easing: this.state.easing,
                useNativeDriver: true,
            })
        ]).start();
    }

    ChangePage(){
        this.props.ChangePageToPayroll();
        this.AnimateHide();
    }

    render(){
        return(
            <View style = {styles.container}>
                <View style = {styles.viewport}>
                    <Animated.View style = {[styles.homecontainer, {opacity: this.state.animateHeader.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateY: this.state.animateHeader.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-10, 0, 0]
                        })}, {translateX: this.state.animateHeader.interpolate({
                            inputRange: [100, 200],
                            outputRange: [0, -20]
                        })}]}]}>
                            <Text style = {styles.home}>Home</Text>
                        </Animated.View>
                        <Animated.View style = {[styles.payrollcontainer, {opacity: this.state.animatePayroll.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateY: this.state.animatePayroll.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [10, 0, 0]
                        })}, {translateX: this.state.animatePayroll.interpolate({
                            inputRange: [100, 200],
                            outputRange: [0, -20]
                        })}]}]}>
                            <View style = {styles.payrollheadercontainer}>
                                <Text style = {styles.payrollheader}>Payroll</Text>
                                <Text style = {styles.payrolldescriptioncontainer}>Manage your payslips, loans, fees and more. </Text>
                            </View>
                            <TouchableWithoutFeedback onPress = {this.ChangePage.bind(this)}>
                                <View style = {styles.payrollarrowcontainer}>
                                    <Text style = {styles.payrollarrow}>Explore</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <View style = {styles.payrollbackgroundcontainer}>
                                <ImageBackground source = {require('./resources/payroll.jpg')} style = {styles.payrollbackground} />
                                <View style = {styles.filter} />
                            </View>
                        </Animated.View>
                        <Animated.View style = {[styles.box, styles.boxone, {opacity: this.state.animateBoxOne.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateX: this.state.animateBoxOne.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-10, 0, -20]
                        })}]}]}>
                            
                        </Animated.View>
                        <Animated.View style = {[styles.box, styles.boxtwo, {opacity: this.state.animateBoxTwo.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateX: this.state.animateBoxTwo.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-10, 0, -20]
                        })}]}]}>
                            
                        </Animated.View>
                        <Animated.View style = {[styles.box, styles.boxthree, {opacity: this.state.animateBoxThree.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateX: this.state.animateBoxThree.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-10, 0, -20]
                        })}]}]}>
                            
                        </Animated.View>
                        <Animated.View style = {[styles.box, styles.boxfour, {opacity: this.state.animateBoxfour.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateX: this.state.animateBoxThree.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [-10, 0, -20]
                        })}]}]}>
                            
                        </Animated.View>
                        <Animated.View style = {[styles.sidepanel, {opacity: this.state.animateSidePanel.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [0, 1, 0]
                        }), transform: [{translateX: this.state.animateSidePanel.interpolate({
                            inputRange: [0, 100, 200],
                            outputRange: [10, 0, -20]
                        })}]}]}>

                        </Animated.View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    viewport: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
    },
    homecontainer: {
        position: 'absolute',
        left: '5%',
        top: '10%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    home: {
        color: '#323232',
        fontFamily: 'Nunito-Black',
        fontSize: DynamicFont(100)
    },
    payrollcontainer: {
        height: '25%',
        width: '90%',
        position: 'absolute',
        left: '5%',
        top: '20%',
        borderRadius: GetScreenHeight(1),
        backgroundColor: 'white',
        elevation: 2,
        overflow: 'hidden',
    },
    payrollheadercontainer: {
        position: 'absolute',
        left: '5%',
        top: '5%',
        width: '55%',
    },
    payrollheader: {
        color: '#323232',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: DynamicFont(65),
        width: '100%',
    },
    payrolldescriptioncontainer: {
        width: '100%',
        fontSize: DynamicFont(45),
        color: '#969696',
        fontFamily: 'Montserrat-Regular',
        top: '10%',
    },
    payrollarrowcontainer: {
        width: '45%',
        height: GetScreenHeight(5),
        position: 'absolute',
        left: '5%',
        bottom: '5%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#323232',
        borderRadius: GetScreenHeight(1),
    },
    payrollarrow: {
        color: 'white',
        fontSize: DynamicFont(45),
        fontFamily: 'Nunito-Regular',
    },
    payrollbackgroundcontainer: {
        height: '100%',
        width: '45%',
        position: 'absolute',
        right: '0%',
        top: '0%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    payrollbackground: {
        height: '100%',
        width: '100%',
        position: 'absolute',
    },
    filter: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        left: '0%',
        top: '0%',
        backgroundColor: '#323232',
        opacity: 0.5,
    },
    box: {
        width: '20.625%',
        height: GetScreenWidth(20.625),
        position: 'absolute',
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
    boxone: {
        left: '5%',
        top: GetScreenHeight(45),
    },
    boxtwo: {
        left: '28.125%',
        top: GetScreenHeight(45),
        backgroundColor: '#646464'
    },
    boxthree: {
        left: '5%',
        top: GetScreenHeight(45) + GetScreenWidth(20.625) + GetScreenHeight(1.5),
        backgroundColor: '#646464',
    },
    boxfour: {
        left: '28.125%',
        top: GetScreenHeight(45) + GetScreenWidth(20.625) + GetScreenHeight(1.5),
    },
    sidepanel: {
        height: '45%',
        width: '42.5%',
        position: 'absolute',
        left: '52.5%',
        top: '47.5%',
        backgroundColor: 'white',
        borderRadius: GetScreenHeight(1),
        elevation: 2,
    },
})